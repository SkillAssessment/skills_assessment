import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from '../data.service';
import { Router, NavigationEnd } from '@angular/router';
import { MatSort, MatPaginator, MatTableDataSource } from '@angular/material';
import { Skill_rate } from '../skill';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-skill-ratings',
  templateUrl: './skill-ratings.component.html',
  styleUrls: ['./skill-ratings.component.scss']
})
export class SkillRatingsComponent implements OnInit {

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  currentUrl: string;
  evaluator_id = localStorage.getItem('emp_id');
  manager_name = localStorage.getItem('name');
  token = localStorage.getItem('token');
  token_id = 'Token ' + this.token;
  skill = new FormControl();
  rating = new FormControl();
  skills: object;
  employee_id: string;
  inputValue: number;
  rate: Skill_rate[] = [];

  constructor(private data: DataService, private router: Router, private spinner: NgxSpinnerService) {
    router.events.subscribe((_: NavigationEnd) => this.currentUrl = _.url);
  }
  emp_ratings: any;
  displayedColumns: string[] = ['emp_id', 'emp_name', 'skill_name', 'rating', 'evaluated_at'];
  dataSource = new MatTableDataSource();

  ngOnInit() {
    this.spinner.show();
    this.data.getSkills(this.token_id).subscribe(
      data => {
        this.skills = data['skill_details'];
      }
    );
    this.data.get_emp_ratings(this.evaluator_id, this.token_id).subscribe(
      data => {
        // console.log(data);
        this.emp_ratings = data['emp_ratings'];
        console.log(this.emp_ratings);
        this.dataSource = new MatTableDataSource(this.emp_ratings);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        setTimeout(() => {
          this.spinner.hide();
      });
      }
    );
  }

  serachSkillRate(skillid, skillrate) {
    this.spinner.show();
    this.data.serachSkillRate(this.evaluator_id, skillid, skillrate, this.token_id).subscribe(
      data => {
        console.log(data);
        this.emp_ratings = data['emp_ratings'];
        setTimeout(() => {
          /** spinner ends after 5 seconds */
          this.spinner.hide();
      });
        if (this.emp_ratings == null) {
        } else {
          console.log(skillid);
          console.log(skillrate);
          this.dataSource = new MatTableDataSource(this.emp_ratings);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        }
      }
    );
  }

}
