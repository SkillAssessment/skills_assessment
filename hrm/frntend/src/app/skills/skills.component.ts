import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { DataService } from '../data.service';
import { Router, NavigationEnd } from '@angular/router';
import { Skill } from '../skill';
import { MyErrorStateMatcher } from 'src/app/default.error-matcher';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.scss']
})

export class SkillsComponent implements OnInit {

  status: string;

  // tslint:disable-next-line:max-line-length
  constructor(private data: DataService, private router: Router, private spinner: NgxSpinnerService) {
    router.events.subscribe((_: NavigationEnd) => this.currentUrl = _.url);
  }

  str: String;
  skill = new FormControl();
  abc = new FormControl();
  skills: object;
  evaluatordetails: object;
  type: Skill[] = [];
  currentUrl: string;
  public mySentences: Array<Object>;
  matcher = new MyErrorStateMatcher();
  invalid: any;
  returnUrl: string;

  id = localStorage.getItem('emp_id');
  token = localStorage.getItem('token');
  token_id = 'Token ' + this.token;

  ngOnInit() {

    this.returnUrl = '/login';

    this.spinner.show();
    this.data.getSkills(this.token_id).subscribe(
      data => {
        this.skills = data['skill_details'];
        // this.invalid = data['detail'];
        // if (this.invalid === 'Invalid token') {
        //   this.router.navigate([this.returnUrl]);
        // }
      }
    );

    this.data.getevaluator(this.id, this.token_id).subscribe(
      data => {
        this.evaluatordetails = data['manager_details'];
        // console.log(data);
        setTimeout(() => {
          /** spinner ends after 2 seconds */
          this.spinner.hide();
      });
      }
    );
  }
  detailsChange(value) {
    // console.log(value);
  }
  skillsChange(abcd) {
    console.log(abcd);
  }

  postSkills(skilldetail, evaluatordet) {

    this.spinner.show();
    this.str = evaluatordet['manager_id'];
    this.type = [];
    for (const i of skilldetail) {
      this.type.push({
        id: i['skill_id']
      });

    }

    this.data.postSkills(this.type, this.str, this.id, this.token_id).subscribe(

      data => {
        console.log('data');
        this.status = data['status'];
        // this.alerts.setMessage(data['status'], 'success');
        setTimeout(() => {
          this.spinner.hide();
      });
      }
    );

  }

}

