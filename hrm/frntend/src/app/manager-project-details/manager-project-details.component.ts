import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from '../data.service';
import { Router, NavigationEnd } from '@angular/router';
import { MatSort, MatPaginator } from '@angular/material';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-manager-project-details',
  templateUrl: './manager-project-details.component.html',
  styleUrls: ['./manager-project-details.component.scss']
})
export class ManagerProjectDetailsComponent implements OnInit {


  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  currentUrl: string;
  projects: string;
  id = localStorage.getItem('emp_id');
  email_id: string;

  token = localStorage.getItem('token');
  token_id = 'Token ' + this.token;

  constructor(private data: DataService, private router: Router, private spinner: NgxSpinnerService) {
    router.events.subscribe((_: NavigationEnd) => this.currentUrl = _.url);
  }
  displayedColumns: string[] = ['project_name', 'manager_name', 'started_on', 'project_status', 'completed_on', 'emp_list'];

  ngOnInit() {
    this.spinner.show();
    this.data.getManagerprojectsdetails(this.id, this.token_id).subscribe(
      data => {
        console.log(data);
        this.projects = data['projects_list'];
        setTimeout(() => {
          this.spinner.hide();
      });
        for (const i of this.projects) {
          console.log(i['emp_list']);
        }
        console.log(data['projects_list']);
      }
    );
  }
}

