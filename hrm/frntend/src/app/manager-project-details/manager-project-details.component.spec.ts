import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagerProjectDetailsComponent } from './manager-project-details.component';

describe('ManagerProjectDetailsComponent', () => {
  let component: ManagerProjectDetailsComponent;
  let fixture: ComponentFixture<ManagerProjectDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagerProjectDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagerProjectDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
