import { Component } from '@angular/core';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup, FormBuilder } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { Router } from '@angular/router';
import { DataService } from '../data.service';
import { NgxSpinnerService } from 'ngx-spinner';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const invalidCtrl = !!(control && control.invalid && control.parent.dirty);
    const invalidParent = !!(control && control.parent && control.parent.invalid && control.parent.dirty);
    return (invalidCtrl || invalidParent);
  }
}

/** @title Input with a custom ErrorStateMatcher */
@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.component.html',
  styleUrls: ['./changepassword.component.scss']
})
export class ChangepasswordComponent {
  myForm: FormGroup;
  response: any;

  matcher = new MyErrorStateMatcher();

  emp_id = localStorage.getItem('emp_id');
  token = localStorage.getItem('token');
  token_id = 'Token ' + this.token;

  hide = true;

  constructor(private formBuilder: FormBuilder, private data: DataService, private router: Router, private spinner: NgxSpinnerService) {
    this.myForm = this.formBuilder.group({
      oldpassword: ['', [Validators.required]],
      password: ['', [Validators.required]],
      confirmPassword: ['']
    }, { validator: this.checkPasswords });

  }

  checkPasswords(group: FormGroup) { // here we have the 'passwords' group
    const pass = group.controls.password.value;
    const confirmPass = group.controls.confirmPassword.value;
    return pass === confirmPass ? null : { notSame: true };
  }

  changePassword(oldpass, newpass) {
    this.spinner.show();
    this.data.changePassword(this.emp_id, oldpass, newpass, this.token_id).subscribe(
      data => {
        console.log(data);
        this.response = data['response'];
        setTimeout(() => {
          this.spinner.hide();
      });
        // this.router.navigateByUrl['login'];
      }
    );
  }
}
