import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DataService } from '../data.service';
import { Router, NavigationEnd } from '@angular/router';
import { MyErrorStateMatcher } from 'src/app/default.error-matcher';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-manger-interviwer',
  templateUrl: './manger-interviwer.component.html',
  styleUrls: ['./manger-interviwer.component.scss']
})
export class MangerInterviwerComponent implements OnInit {
  knowledge_model: string;
  verbal_model: string;
  type_model: string;
  attitude_model: string;
  recommandation_model: string;

  name = new FormControl();
  date_of_interview = new FormControl();
  emailid = new FormControl('', [Validators.required, Validators.email]);
  Experience = new FormControl();
  phone_no = new FormControl();
  notice_period = new FormControl();
  comments = new FormControl();
  rate = new FormControl();

  matcher = new MyErrorStateMatcher();

  knowledge = [
    'Excellent',
    'Good',
    'Average',
    'BelowAverage'
  ];
  verbal = [
    'Excellent',
    'Good',
    'Average',
    'BelowAverage'
  ];
  type = [
    'Consultant',
    'Employee',
    'Internship'
  ];
  attitude = [
    'Matches AmberTAG',
    'May need some time to understand AmberTAG',
    'Will not be a match'
  ];
  recommandations = [
    'Selected',
    'Rejected',
    'Hold for future reference'

  ];
  token = localStorage.getItem('token');
  token_id = 'Token ' + this.token;
  currentUrl: string;
  status: any;
  constructor(private data: DataService, private router: Router, private spinner: NgxSpinnerService) {
    router.events.subscribe((_: NavigationEnd) => this.currentUrl = _.url);
  }

  ngOnInit() {
  }

  // tslint:disable-next-line:max-line-length
  postInterviewer_data(getname, getemailid, exp_data, getphone_no, knowledge, verbal, type, notice_period, comments, attitude, recommandation, average) {
    this.spinner.show();
    // tslint:disable-next-line:max-line-length
    this.data.postInterviewer_data(getname, getemailid, exp_data, getphone_no, knowledge, verbal, type, notice_period, comments, attitude, recommandation, average, this.token_id).subscribe(
      data => {
        console.log(data);
        this.status = data['status'];
        setTimeout(() => {
          this.spinner.hide();
      });
      }
    );

  }
}


