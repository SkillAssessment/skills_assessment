import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MangerInterviwerComponent } from './manger-interviwer.component';

describe('MangerInterviwerComponent', () => {
  let component: MangerInterviwerComponent;
  let fixture: ComponentFixture<MangerInterviwerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MangerInterviwerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MangerInterviwerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
