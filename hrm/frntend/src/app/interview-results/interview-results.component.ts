import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from '../data.service';
import { NavigationEnd, Router } from '@angular/router';
import { AlertsService } from 'angular-alert-module';
import { FormControl } from '@angular/forms';
import { MatSort, MatPaginator, MatTableDataSource } from '@angular/material';
import { MyErrorStateMatcher } from 'src/app/default.error-matcher';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-interview-results',
  templateUrl: './interview-results.component.html',
  styleUrls: ['./interview-results.component.scss']
})
export class InterviewResultsComponent implements OnInit {

  currentUrl: string;

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  token = localStorage.getItem('token');
  token_id = 'Token ' + this.token;
  projects = [];
  status: any;
  interviewResults: any;

  constructor(private data: DataService, private router: Router, private alerts: AlertsService, private spinner: NgxSpinnerService) {
    router.events.subscribe((_: NavigationEnd) => this.currentUrl = _.url);
  }
  // tslint:disable-next-line:max-line-length
  displayedColumns: string[] = ['interview_id', 'candidate_name', 'candidate_phone_number', 'interview_date', 'email_id', 'experience', 'technical_knowledge', 'verbal_communication_skills', 'type_of_employment', 'notice_period_time', 'comments', 'attitude', 'final_recommendation', 'average_rating'];
  dataSource = new MatTableDataSource();
  // tslint:disable-next-line:member-ordering
  matcher = new MyErrorStateMatcher();

  ngOnInit() {
    this.spinner.show();
    this.data.intervieweeDetails(this.token_id).subscribe(
      data => {
        console.log(data);
        this.interviewResults = data['interview_results'];
        this.dataSource = data['interview_results'];
        this.dataSource = new MatTableDataSource(this.projects);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        setTimeout(() => {
          this.spinner.hide();
      });
        console.log('data');
      }
    );
  }

}
