import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { Router, NavigationEnd } from '@angular/router';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { MyErrorStateMatcher } from 'src/app/default.error-matcher';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.scss']
})
export class AddEmployeeComponent implements OnInit {

  empname = new FormControl('', [Validators.required]);
  firstname = new FormControl('', [Validators.required]);
  empid = new FormControl('', [Validators.required]);
  designation1 = new FormControl('', [Validators.required]);
  date_of_joining = new FormControl('', [Validators.required]);
  email = new FormControl('', [Validators.required, Validators.email]);
  emailid = new FormControl('', [Validators.required, Validators.email]);
  phone_no = new FormControl('', [Validators.required, Validators.minLength(10)]);
  alt_phone_no = new FormControl('', [Validators.required, Validators.minLength(10)]);
  pan_no = new FormControl('', [Validators.required, Validators.minLength(10)]);
  aadhar_no = new FormControl('', [Validators.required, Validators.minLength(12)]);
  father_name = new FormControl('', [Validators.required]);
  father_no = new FormControl('', [Validators.minLength(10)]);
  mother_name = new FormControl('', [Validators.required]);
  mother_no = new FormControl('', [Validators.minLength(10)]);
  emergency_no = new FormControl('', [Validators.required, Validators.minLength(10)]);
  gender = new FormControl('', [Validators.required]);
  date_of_birth = new FormControl('', [Validators.required]);
  permanent_address = new FormControl('', [Validators.required]);
  residential_address = new FormControl('', [Validators.required]);
  qualification = new FormControl('', [Validators.required]);
  blood_group = new FormControl('', [Validators.required]);
  work_timings = new FormControl('', [Validators.required]);
  marital_status = new FormControl('', [Validators.required]);

  designations: object;
  currentUrl: string;
  token = localStorage.getItem('token');
  token_id = 'Token ' + this.token;
  status: string;

  constructor(private data: DataService, private router: Router, private spinner: NgxSpinnerService) {
    router.events.subscribe((_: NavigationEnd) => this.currentUrl = _.url);
  }

  ngOnInit() {
    this.spinner.show();
    this.data.getDesignations(this.token_id).subscribe(
      data => {
        console.log(data);
        this.designations = data['designations'];
        setTimeout(() => {
          this.spinner.hide();
      });
      }
    );
  }

  getErrorMessage() {
      return this.empname.hasError('required') ? 'Invalid' :
             this.firstname.hasError('required') ? 'Invalid' :
             this.empid.hasError('required') ? 'Invalid' :
             this.designation1.hasError('required') ? 'Invalid' :
             this.date_of_joining.hasError('required') ? 'Invalid' :
             this.email.hasError('required') ? 'Invalid' :
             this.email.hasError('email') ? 'Invalid' :
             this.emailid.hasError('required') ? 'Invalid' :
             this.emailid.hasError('emailid') ? 'Invalid' :
             this.phone_no.hasError('required') ? 'Invalid' :
             this.phone_no.hasError('phone_no') ? 'Invalid' :
             this.alt_phone_no.hasError('required') ? 'Invalid' :
             this.alt_phone_no.hasError('alt_phone_no') ? 'Invalid' :
             this.pan_no.hasError('required') ? 'Invalid' :
             this.pan_no.hasError('pan_no') ? 'Invalid' :
             this.aadhar_no.hasError('required') ? 'Invalid' :
             this.aadhar_no.hasError('aadhar_no') ? 'Invalid' :
             this.father_name.hasError('required') ? 'Invalid' :
            //  this.father_no.hasError('required') ? 'Invalid' :
             this.father_no.hasError('father_no') ? 'Invalid' :
             this.mother_name.hasError('required') ? 'Invalid' :
            //  this.mother_no.hasError('required') ? 'Invalid' :
             this.mother_no.hasError('mother_no') ? 'Invalid' :
             this.emergency_no.hasError('required') ? 'Invalid' :
             this.emergency_no.hasError('emergency_no') ? 'Invalid' :
             this.gender.hasError('required') ? 'Invalid' :
             this.date_of_birth.hasError('required') ? 'Invalid' :
             this.permanent_address.hasError('required') ? 'Invalid' :
             this.residential_address.hasError('required') ? 'Invalid' :
             this.blood_group.hasError('required') ? 'Invalid' :
             this.work_timings.hasError('required') ? 'Invalid' :
             this.marital_status.hasError('required') ? 'Invalid' :
              '';
    }
    // tslint:disable-next-line:member-ordering
    matcher = new MyErrorStateMatcher();

    // tslint:disable-next-line:max-line-length
    addEmployee(Employeename, Firstname, EmployeeID, Designation, Dateofjoining, EmailIDpersonal, EmailID, Phonenumber, Alternativephonenumber, PANnumber, Aadharnumber, Fathername, Fathernumber, Mothername, Mothernumber, Emergencynumber, Gender, Dateofbirth, Permanentaddress, Residentialaddress, Qualification, Bloodgroup, Worktimings, Maritalstatus) {
      this.spinner.show();
      console.log(Designation);
      console.log(Aadharnumber);
      // tslint:disable-next-line:max-line-length
    this.data.addEmployee(Employeename, Firstname, EmployeeID, Designation, Dateofjoining, EmailIDpersonal, EmailID, Phonenumber, Alternativephonenumber, PANnumber, Aadharnumber, Fathername, Fathernumber, Mothername, Mothernumber, Emergencynumber, Gender, Dateofbirth, Permanentaddress, Residentialaddress, Qualification, Bloodgroup, Worktimings, Maritalstatus, this.token_id).subscribe(
      data => {
        this.status = data['status'];
        setTimeout(() => {
          this.spinner.hide();
      });
      });
}
}
