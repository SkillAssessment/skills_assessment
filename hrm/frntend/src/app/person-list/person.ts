export class Person {
  name: string;
  age: string;
  status: string;
  designation: string;
  date_of_promotion: string;
  date_of_joining: number;
  months_on_job: string;
  phone_number: number;
  alternative_phone_number: string;
  email: string;
  email_id_personal: string;
  pan_number: number;
  aadhaar_number: number;
  father_name: string;
  father_number: number;
  mother_name: string;
  mother_number: number;
  emergency_contact_number: number;
  gender: string;
  date_of_birth: string;
  permanent_address: string;
  resident_address: string;
  qualification: string;
  date_of_resigning: string;
  date_of_rejoining: string;
  blood_group: string;
  marital_status: string;

}

