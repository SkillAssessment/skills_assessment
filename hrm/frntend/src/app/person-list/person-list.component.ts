import { Component, Input, Output, EventEmitter, OnInit, ViewChild } from '@angular/core';
import { TableDataSource, ValidatorService } from 'angular4-material-table';

import { PersonValidatorService } from './person-validator.service';
import { Person } from './person';
import { DataService } from '../data.service';
import { Router, NavigationEnd } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { MatSort, MatPaginator, MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-person-list',
  providers: [
    { provide: ValidatorService, useClass: PersonValidatorService }
  ],
  templateUrl: './person-list.component.html',
  styleUrls: ['./person-list.component.scss']
})
export class PersonListComponent implements OnInit {

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  personal_details = [];
  currentUrl: string;

  // constructor(private personValidator: ValidatorService) { }
  // tslint:disable-next-line:max-line-length
  constructor(private data: DataService, private router: Router, private spinner: NgxSpinnerService, private personValidator: ValidatorService) {
    router.events.subscribe((_: NavigationEnd) => this.currentUrl = _.url);
  }
  token = localStorage.getItem('token');
  token_id = 'Token ' + this.token;
  // tslint:disable-next-line:max-line-length
  displayedColumns = ['emp_id', 'name', 'gender', 'date_of_birth', 'email', 'email_id_personal', 'phone_number', 'alternative_phone_number', 'permanent_address', 'resident_address', 'blood_group', 'marital_status', 'pan_number', 'aadhaar_number', 'father_name', 'father_number', 'mother_name', 'mother_number', 'emergency_contact_number', 'qualification', 'designation', 'date_of_joining', 'date_of_promotion', 'months_on_job', 'date_of_resigning', 'date_of_rejoining', 'status', 'actionsColumn'];

  // displayedColumns = ['name', 'emp_id','status', 'designation'];
  @Output() personListChange = new EventEmitter<Person[]>();

  dataSource: TableDataSource<Person>;


  ngOnInit() {
    this.spinner.show();
    this.data.getallemployeespersonalinfo(this.token_id).subscribe(
      data => {
        console.log(data);
        this.personal_details = data['emp_details'];
        this.dataSource = data['emp_details'];


        this.dataSource = new TableDataSource<any>(this.personal_details, Person, this.personValidator);

        this.dataSource.datasourceSubject.subscribe(personal_details => this.personListChange.emit(personal_details));
        setTimeout(() => {
          this.spinner.hide();
        });
      });
  }

  // tslint:disable-next-line:max-line-length
  confirmEditCreate(emp_id, name, gender, date_of_birth, email, email_id_personal, phone_number, alternative_phone_number, permanent_address, resident_address, blood_group, marital_status, pan_number, aadhaar_number, father_name, father_number, mother_name, mother_number, emergency_contact_number, designation, qualification, date_of_joining, months_on_job, date_of_resigning, date_of_rejoining, status) {
    console.log(name);
    console.log(emp_id);
    console.log(gender);
    console.log(date_of_birth);
    console.log(email);
    console.log(email_id_personal);
    console.log(phone_number);
    console.log(alternative_phone_number);
    console.log(permanent_address);
    console.log(resident_address);
    console.log(blood_group);
    console.log(pan_number);
    console.log(aadhaar_number);
    console.log(designation);
    this.spinner.show();
    // tslint:disable-next-line:max-line-length
    this.data.confirmEditCreate(emp_id, name, gender, date_of_birth, email, email_id_personal, phone_number, alternative_phone_number, permanent_address, resident_address, blood_group, marital_status, pan_number, aadhaar_number, father_name, father_number, mother_name, mother_number, emergency_contact_number, designation, qualification, date_of_joining, months_on_job, date_of_resigning, date_of_rejoining, status, this.token_id).subscribe(
      data => {
        console.log(data);
        setTimeout(() => {
          this.spinner.hide();
        });
      });
  }

  postEmployeeid(empid) {
    this.data.postEmployeeid(empid, this.token_id).subscribe(
      data => {
        console.log(data);
        console.log(this.token);
        this.personal_details = data['emp_details'];
        this.dataSource = data['emp_details'];


        this.dataSource = new TableDataSource<any>(this.personal_details, Person, this.personValidator);

        this.dataSource.datasourceSubject.subscribe(personal_details => this.personListChange.emit(personal_details));

      });
  }
}
