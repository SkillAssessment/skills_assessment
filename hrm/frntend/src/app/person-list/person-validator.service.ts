import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ValidatorService } from 'angular4-material-table';

@Injectable()
export class PersonValidatorService implements ValidatorService {
  getRowValidator(): FormGroup {
    return new FormGroup({
      'name': new FormControl(),
      'emp_id': new FormControl(),
      'status': new FormControl(),
      'designation': new FormControl(),
      'date_of_promotion': new FormControl(),
      'date_of_joining': new FormControl(),
      'months_on_job': new FormControl(),
      'phone_number': new FormControl(),
      'alternative_phone_number': new FormControl(),
      'email': new FormControl(),
      'email_id_personal': new FormControl(),
      'pan_number': new FormControl(),
      'aadhaar_number': new FormControl(),
      'father_name': new FormControl(),
      'father_number': new FormControl(),
      'mother_name': new FormControl(),
      'mother_number': new FormControl(),
      'emergency_contact_number': new FormControl(),
      'gender': new FormControl(),
      'date_of_birth': new FormControl(),
      'permanent_address': new FormControl(),
      'resident_address': new FormControl(),
      'qualification': new FormControl(),
      'date_of_resigning': new FormControl(),
      'date_of_rejoining': new FormControl(),
      'blood_group': new FormControl(),
      'marital_status': new FormControl()

    });
  }
}

