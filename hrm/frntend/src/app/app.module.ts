import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CdkTableModule } from '@angular/cdk/table';
import { CdkTreeModule } from '@angular/cdk/tree';

import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AlertsModule } from 'angular-alert-module';
import { AuthGuard } from './auth.guard';
import { NgxSpinnerModule } from 'ngx-spinner';

import { LoginComponent } from './login/login.component';
import { SkillsComponent } from './skills/skills.component';
import { ProjectDetailsComponent } from './project-details/project-details.component';
import { PersonalDetailsComponent } from './personal-details/personal-details.component';
import { ManagerProjectDetailsComponent } from './manager-project-details/manager-project-details.component';
import { SkillAssessmentComponent } from './skill-assessment/skill-assessment.component';
import { EmployeeCategoriesComponent } from './employee-categories/employee-categories.component';
import { AllProjectsInfoComponent } from './all-projects-info/all-projects-info.component';
import { HomeComponent } from './home/home.component';
import { AllSkillRatingsComponent } from './all-skill-ratings/all-skill-ratings.component';
import { InterviwerComponent } from './interviwer/interviwer.component';
import { MangerInterviwerComponent } from './manger-interviwer/manger-interviwer.component';
import { ChangepasswordComponent } from './changepassword/changepassword.component';
import { AddProjectComponent } from './add-project/add-project.component';
import { AddEmployeeComponent } from './add-employee/add-employee.component';
import { InterviewResultsComponent } from './interview-results/interview-results.component';
import { MySkillsComponent } from './my-skills/my-skills.component';
import { AddNewSkillComponent } from './add-new-skill/add-new-skill.component';
import { SkillRatingsComponent } from './skill-ratings/skill-ratings.component';
import { AddDesignationComponent } from './add-designation/add-designation.component';
import { LoginMenuComponent } from './login-menu/login-menu.component';
import { MainMenuComponent } from './main-menu/main-menu.component';
import { PersonListComponent } from './person-list/person-list.component';

import {

  MatAutocompleteModule,
  MatBadgeModule,
  MatBottomSheetModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatTreeModule,
  MatFormFieldModule,
} from '@angular/material';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SkillsComponent,
    ProjectDetailsComponent,
    PersonalDetailsComponent,
    SkillAssessmentComponent,
    ManagerProjectDetailsComponent,
    EmployeeCategoriesComponent,
    AllProjectsInfoComponent,
    AllSkillRatingsComponent,
    InterviwerComponent,
    MangerInterviwerComponent,
    HomeComponent,
    ChangepasswordComponent,
    AddProjectComponent,
    AddEmployeeComponent,
    InterviewResultsComponent,
    MySkillsComponent,
    AddNewSkillComponent,
    SkillRatingsComponent,
    AddDesignationComponent,
    PersonListComponent,
    LoginMenuComponent,
    MainMenuComponent
  ],

  imports: [
    AlertsModule.forRoot(),
    NgxSpinnerModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    CdkTableModule,
    CdkTreeModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
  ],

  exports: [
    MatFormFieldModule,
    CdkTableModule,
    CdkTreeModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
  ],

  bootstrap: [AppComponent],
  providers: [AuthGuard],
})
export class AppModule { }
