import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort, MatPaginator } from '@angular/material';
import { DataService } from '../data.service';
import { Router, NavigationEnd } from '@angular/router';
import { Skill } from '../skill';
import { NgxSpinnerService } from 'ngx-spinner';
import { ErrorStateMatcher } from '@angular/material/core';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    return !!(control && control.invalid && (control.dirty || control.touched));
  }
}

@Component({
  selector: 'app-all-skill-ratings',
  templateUrl: './all-skill-ratings.component.html',
  styleUrls: ['./all-skill-ratings.component.scss']
})
export class AllSkillRatingsComponent implements OnInit {


  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  currentUrl: string;
  skills: string;
  email_id: string;
  emp_details: object;

  skills_details: string;

  token = localStorage.getItem('token');
  token_id = 'Token ' + this.token;

  type: Skill[] = [];

  skill = new FormControl('', [Validators.required]);

  constructor(private data: DataService, private router: Router, private spinner: NgxSpinnerService) {
    router.events.subscribe((_: NavigationEnd) => this.currentUrl = _.url);
  }

  displayedColumns: string[] =
    // tslint:disable-next-line:max-line-length
    ['skill_name'];

    // tslint:disable-next-line:member-ordering
  matcher = new MyErrorStateMatcher();


  getErrorMessage() {
    return this.skill.hasError('required') ? 'Field is Required' :
            '';
  }

  ngOnInit() {
    this.spinner.show();

    this.data.getSkills(this.token_id).subscribe(
      data => {
        this.skills = data['skill_details'];
      }
    );
    console.log(this.token_id);
    this.data.getAllskillRating(this.token_id).subscribe(
      data => {
        console.log(data);
        this.skills_details = data['skill_list'];
        setTimeout(() => {
          this.spinner.hide();
      });
      }
    );
  }

  postSkillid(skillid) {
    // console.log(skillid);
    this.spinner.show();

    this.type = [];
    for (const i of skillid) {
      console.log(skillid);
      this.type.push({
        id: i['skill_id']
      });

    }
    console.log(this.type);

    this.data.postSkillid(this.type, this.token_id).subscribe(

      data => {

        console.log(data);
        this.skills_details = data['skill_list'];
        setTimeout(() => {
          this.spinner.hide();
      });
      }
    );

  }

}


