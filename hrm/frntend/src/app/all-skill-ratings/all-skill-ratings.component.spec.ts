import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllSkillRatingsComponent } from './all-skill-ratings.component';

describe('AllSkillRatingsComponent', () => {
  let component: AllSkillRatingsComponent;
  let fixture: ComponentFixture<AllSkillRatingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllSkillRatingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllSkillRatingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
