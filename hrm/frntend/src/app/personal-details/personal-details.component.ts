import { Component, OnInit} from '@angular/core';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { DataService } from '../data.service';
import { NgxSpinnerService } from 'ngx-spinner';

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    return !!(control && control.invalid && (control.dirty || control.touched));
  }
}

@Component({
  selector: 'app-personal-details',
  templateUrl: './personal-details.component.html',
  styleUrls: ['./personal-details.component.scss']
})
export class PersonalDetailsComponent implements OnInit {
  hide_button: boolean;
  hide_setting: boolean;

  emp_id = localStorage.getItem('emp_id');
  token = localStorage.getItem('token');
  token_id = 'Token ' + this.token;

  constructor(private data: DataService, private spinner: NgxSpinnerService) { }
  id: any;
  name: any;
  designation: any;
  gender: any;
  date_of_birth: any;
  father_name: any;
  father_number: any;
  mother_name: any;
  mother_number: any;
  permanent_address: any;
  resident_address: any;
  marital_status: any;
  email: any;
  email_id_personal: any;
  public phone_number: any;
  alternative_phone_number: any;
  pan_number: any;
  aadhaar_number: any;
  qualification: any;
  date_of_joining: any;
  emergency_contact_number: any;
  blood_group: any;
  status: string;

  emailid = new FormControl('', [Validators.email]);
  Phonenumber = new FormControl('', [Validators.minLength(10)]);
  Altphonenumber = new FormControl('', [Validators.minLength(10)]);
  Fathernumber = new FormControl('', [Validators.minLength(10)]);
  Mothernumber = new FormControl('', [Validators.minLength(10)]);
  Maritalstatus = new FormControl('', [Validators.email]);
  Emergencynumber = new FormControl('', [Validators.email]);
  Bloodgroup = new FormControl('', [Validators.email]);

  ngOnInit() {

    this.hide_setting = true;
      this.id = localStorage.getItem('emp_id');
      this.name = localStorage.getItem('name');
      this.designation = localStorage.getItem('designation');
      this.gender = localStorage.getItem('gender');
      this.date_of_birth = localStorage.getItem('date_of_birth');
      this.father_name = localStorage.getItem('father_name');
      this.father_number = localStorage.getItem('father_number');
      this.mother_name = localStorage.getItem('mother_name');
      this.mother_number = localStorage.getItem('mother_number');
      this.permanent_address = localStorage.getItem('permanent_address');
      this.resident_address = localStorage.getItem('resident_address');
      this.marital_status = localStorage.getItem('marital_status');
      this.email = localStorage.getItem('email');
      this.email_id_personal = localStorage.getItem('email_id_personal');
      this.phone_number = localStorage.getItem('phone_number');
      this.alternative_phone_number = localStorage.getItem('alternative_phone_number');
      this.pan_number = localStorage.getItem('pan_number');
      this.aadhaar_number = localStorage.getItem('aadhaar_number');
      this.qualification = localStorage.getItem('qualification');
      this.date_of_joining = localStorage.getItem('date_of_joining');
      this.emergency_contact_number = localStorage.getItem('emergency_contact_number');
      this.blood_group = localStorage.getItem('blood_group');
  }
  getErrorMessage() {
    return this.emailid.hasError('email') ? 'Invalid' :
        this.Phonenumber.hasError('Phonenumber') ? 'Invalid' :
        this.Altphonenumber.hasError('Altphonenumber') ? 'Invalid' :
            '';
  }
  // tslint:disable-next-line:member-ordering
  matcher = new MyErrorStateMatcher();

  editDetails(emailid, Phonenumber, Altphonenumber) {

    this.spinner.show();
    //localStorage.removeItem('phone_number');
    this.data.editDetails(this.emp_id, emailid, Phonenumber, Altphonenumber, this.token_id).subscribe(
      data => {
        console.log("data");
         console.log("data");
        this.phone_number = data['phone_number'];
        console.log("1");
        console.log(this.phone_number);
        window.location.reload();
        this.status = data['status'];
        console.log("2");
        
        setTimeout(() => {
          this.spinner.hide();
      });
      });
}

editDetails2(Fathernumber, Mothernumber, Maritalstatus, Emergencynumber, Bloodgroup) {
  this.spinner.show();
  this.data.editDetails2(this.emp_id, Fathernumber, Mothernumber, Maritalstatus, Emergencynumber, Bloodgroup, this.token_id).subscribe(
    data => {
      this.status = data['status'];
      setTimeout(() => {
        this.spinner.hide();
    });
    });
}
}
