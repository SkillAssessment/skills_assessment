import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from '../data.service';
import { Router, NavigationEnd } from '@angular/router';
import { AlertsService } from 'angular-alert-module';
import { MyErrorStateMatcher } from 'src/app/default.error-matcher';
import { Skill_rate } from '../skill';
import { FormControl } from '@angular/forms';
import { MatSort, MatPaginator, MatTableDataSource } from '@angular/material';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-skill-assessment',
  templateUrl: './skill-assessment.component.html',
  styleUrls: ['./skill-assessment.component.scss']
})
export class SkillAssessmentComponent implements OnInit {


  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  currentUrl: string;
  evaluator_id = localStorage.getItem('emp_id');
  manager_name = localStorage.getItem('name');
  token = localStorage.getItem('token');
  token_id = 'Token ' + this.token;

  skill_name: string;
  emp_name: string;
  emp_id: number;
  skill_assess: string;
  rating: string;
  inputValue: number;
  rate: Skill_rate[] = [];
  skill_rate: number;
  skill_id: number;
  aaa = new FormControl();
  employee_id: string;
  all_skill_rate: any;
  rate_details: any;
  emp_ratings: any;
  skills: object;
  skill = new FormControl();
  number = new FormControl();
  skillrate: number;
  skillid: number;
  status:string;

  constructor(private data: DataService, private router: Router, private alerts: AlertsService, private spinner: NgxSpinnerService) {
    router.events.subscribe((_: NavigationEnd) => this.currentUrl = _.url);
  }
  displayedColumns: string[] = ['emp_id', 'emp_name', 'skill_name', 'rating', 'evaluated_at'];
  dataSource = new MatTableDataSource();

  matcher = new MyErrorStateMatcher();
  ngOnInit() {
    this.spinner.show();
    this.data.getSkills(this.token_id).subscribe(
      data => {
        this.skills = data['skill_details'];
      }
    );

    this.data.get_empskills_details(this.evaluator_id, this.token_id).subscribe(
      data => {
        // console.log(data);
        this.skill_assess = data['emp_details'];
        this.skill_assess = data['emp_details'];
        setTimeout(() => {
          this.spinner.hide();
      });
        for (const i of this.skill_assess) {
          this.rating = i['skill_details'];
        }
      });

    this.data.get_emp_ratings(this.evaluator_id, this.token_id).subscribe(
      data => {
        // console.log(data);
        this.emp_ratings = data['emp_ratings'];
        console.log(this.emp_ratings);
        this.dataSource = new MatTableDataSource(this.emp_ratings);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      }
    );
  }
  onKey(event, id, employee_id) {
    this.employee_id = employee_id;
    this.inputValue = event.target.value;
    this.rate.push({
      skill_id: id,
      skill_rating: this.inputValue
    });
    console.log(this.rate);
  }
  postSkillRate() {
    this.spinner.show();
    console.log("data");

    this.data.postSkillRate(this.rate, this.evaluator_id, this.employee_id, this.token_id).subscribe(
      data => {
        console.log(data);
        console.log(this.evaluator_id);
        this.status=data['status'];
        setTimeout(() => {
          this.spinner.hide();
      });
      }
    );
  }
  serachSkillRate(skillid, skillrate) {
    console.log('data');
    this.data.serachSkillRate(this.evaluator_id, skillid, skillrate, this.token_id).subscribe(
      data => {
        console.log(data);
        this.emp_ratings = data['emp_ratings'];
        if (this.emp_ratings == null) {
          alert('kdfhgkj');
        } else {
          console.log(skillid);
          console.log(skillrate);
          this.dataSource = new MatTableDataSource(this.emp_ratings);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        }
      }
    );
  }
}



