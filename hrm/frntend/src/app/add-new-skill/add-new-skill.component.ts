import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { DataService } from '../data.service';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { NgxSpinnerService } from 'ngx-spinner';

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    return !!(control && control.invalid && (control.dirty || control.touched));
  }
}

@Component({
  selector: 'app-add-new-skill',
  templateUrl: './add-new-skill.component.html',
  styleUrls: ['./add-new-skill.component.scss']
})
export class AddNewSkillComponent implements OnInit {

  token = localStorage.getItem('token');
  token_id = 'Token ' + this.token;
  status: any;
  currentUrl: any;

  constructor(private data: DataService, private router: Router, private spinner: NgxSpinnerService) {
    router.events.subscribe((_: NavigationEnd) => this.currentUrl = _.url);
  }

  skill1 = new FormControl('', [Validators.required]);

  getErrorMessage() {
    return this.skill1.hasError('required') ? 'Field is Required' :
            '';
  }

  ngOnInit() { }
    newskill(skill) {
      this.spinner.show();
      this.data.newskill(skill, this.token_id).subscribe(
        data => {
          this.status = data['status'];
          setTimeout(() => {
            this.spinner.hide();
        });
        });
  }
  // tslint:disable-next-line:member-ordering
  matcher = new MyErrorStateMatcher();

  }
