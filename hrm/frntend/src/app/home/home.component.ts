import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public edited = false;

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );
  sub_role: string;
  hredited: boolean;
  projectedited: boolean;
  manageredited: boolean;
  manager_project: boolean;
  constructor(private data: DataService, private breakpointObserver: BreakpointObserver) { }
  name: any;
  designation: any;
  category = 'manager';

  ngOnInit() {
    this.name = localStorage.getItem('name');
    this.designation = localStorage.getItem('designation');
    this.sub_role = localStorage.getItem('sub_role');
    console.log(this.sub_role);
    if (this.sub_role === '5') {
      this.edited = true;
    } else {
      this.edited = false;
    }
    // tslint:disable-next-line:max-line-length
    if (this.sub_role === '6' || this.sub_role === '5' || this.sub_role === '4' || this.sub_role === '3' || this.sub_role === '2' || this.sub_role === '1') {
      this.projectedited = false;
      this.hredited = false;
    } else {
      this.projectedited = true;
      this.manager_project = true;
      this.hredited = true;
    }
    if (this.sub_role === '4' || this.sub_role === '3' || this.sub_role === '2' || this.sub_role === '1') {
      this.projectedited = true;
    }
    if (this.sub_role === '6') {
      this.hredited = true;
    }
    if (this.sub_role === '7') {
      this.hredited = false;
      this.manageredited = true;
      this.manager_project = true;
      this.projectedited = false;
      this.edited = true;
    }
    if (this.sub_role === '5') {
      this.hredited = false;
     // this.manageredited = true;
      this.manager_project = true;
      this.projectedited = false;
    }
  }
}
