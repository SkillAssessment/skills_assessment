import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllProjectsInfoComponent } from './all-projects-info.component';

describe('AllProjectsInfoComponent', () => {
  let component: AllProjectsInfoComponent;
  let fixture: ComponentFixture<AllProjectsInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllProjectsInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllProjectsInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
