import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from '../data.service';
import { Router, NavigationEnd } from '@angular/router';
import { MatSort, MatPaginator } from '@angular/material';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { Skill, Projects } from '../skill';
import { NgxSpinnerService } from 'ngx-spinner';
import { ErrorStateMatcher } from '@angular/material/core';

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    return !!(control && control.invalid && (control.dirty || control.touched));
  }
}

@Component({
  selector: 'app-all-projects-info',
  templateUrl: './all-projects-info.component.html',
  styleUrls: ['./all-projects-info.component.scss']
})
export class AllProjectsInfoComponent implements OnInit {

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  currentUrl: string;
  projects: string;
  email_id: string;
  token = localStorage.getItem('token');
  token_id = 'Token ' + this.token;

  project: string;
  project_arr: Projects[] = [];

  constructor(private data: DataService, private router: Router, private spinner: NgxSpinnerService) {
    router.events.subscribe((_: NavigationEnd) => this.currentUrl = _.url);
  }

  displayedColumns: string[] =
    // tslint:disable-next-line:max-line-length
    ['project_name', 'manager_name', 'manager_email_id', 'manager_phone_number', 'started_on', 'project_status', 'completed_on', 'primary_cost_center', 'secondary_cost_center', 'emp_details'];


  projects_list = new FormControl('', [Validators.required]);

  getErrorMessage() {
    return this.projects_list.hasError('required') ? 'Field is Required' :
            '';
  }

  // tslint:disable-next-line:member-ordering
  matcher = new MyErrorStateMatcher();


  ngOnInit() {
    this.spinner.show();
    this.data.getAllprojectsdetails(this.token_id).subscribe(
      data => {
        console.log(data['project_details']);
        this.projects = data['project_details'];
        for (const i of this.projects) {
        }
      }
    );

    this.data.getProjects(this.token_id).subscribe(
      data => {
        this.project = data['project_details'];
        setTimeout(() => {
          this.spinner.hide();
      });
      }
    );
  }


  postProjectid(id) {
    this.spinner.show();
    this.project_arr = [];

    for (const i of id) {
      console.log(id);
      this.project_arr.push({
        id: i['project_id']
      });

    }

    this.data.postProjectid(this.token_id, this.project_arr).subscribe(
      data => {
        console.log(this.project_arr);
        this.projects = data['project_details'];
        setTimeout(() => {
          this.spinner.hide();
      });
        for (const i of this.projects) {
          // console.log(i['emp_details']);
        }
        // console.log(data['project_details']);
      }
    );
  }
}


