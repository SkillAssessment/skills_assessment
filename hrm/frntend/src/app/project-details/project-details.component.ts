import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from '../data.service';
import { Router, NavigationEnd } from '@angular/router';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-project-details',
  templateUrl: './project-details.component.html',
  styleUrls: ['./project-details.component.scss']
})

export class ProjectDetailsComponent implements OnInit {

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  projects = [];
  currentUrl: string;
  // projects: object;
  id = localStorage.getItem('emp_id');
  token = localStorage.getItem('token');
  token_id = 'Token ' + this.token;

  constructor(private data: DataService, private router: Router, private spinner: NgxSpinnerService) {
    router.events.subscribe((_: NavigationEnd) => this.currentUrl = _.url);
  }
  displayedColumns: string[] = ['project_name', 'manager_name', 'started_on', 'project_status', 'completed_on'];
  dataSource = new MatTableDataSource();

  ngOnInit() {
    this.spinner.show();
    this.data.getProjectsdetails(this.id, this.token_id).subscribe(
      data => {
        console.log(data);
        this.projects = data['projects_list'];
        this.dataSource = data['projects_list'];
        this.dataSource = new MatTableDataSource(this.projects);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        setTimeout(() => {
          this.spinner.hide();
      });
        console.log('data');
      }
    );
  }


}
