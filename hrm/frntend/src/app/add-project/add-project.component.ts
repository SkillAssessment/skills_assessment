import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from '../data.service';
import { Router, NavigationEnd } from '@angular/router';
import { AlertsService } from 'angular-alert-module';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { MyErrorStateMatcher } from '../default.error-matcher';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { addTeamDetails, EmployeeID, Skill } from '../skill';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-add-project',
  templateUrl: './add-project.component.html',
  styleUrls: ['./add-project.component.scss']
})
export class AddProjectComponent implements OnInit {

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  currentUrl: any;
  managers: string;
  hr: boolean;
  sub_role: string;
  manager: boolean;

  displayedColumns: string[] = ['checked', 'name', 'emp_id', 'skill', 'projects'];
  dataSource = new MatTableDataSource();
  id = localStorage.getItem('emp_id');
  token = localStorage.getItem('token');
  token_id = 'Token ' + this.token;

  skills: any;
  skill = new FormControl('', [Validators.required]);
  Manager = new FormControl('', [Validators.required]);
  project_name = new FormControl('', [Validators.required]);
  Senior_Manager = new FormControl('', [Validators.required]);

  addTeamDetails: addTeamDetails[] = [];

  type: EmployeeID[] = [];

  skilldetails: Skill[] = [];

  skillID = new FormControl();

  abc: object;

  abcd: any;
  server: any;

  constructor(private data: DataService, private router: Router, private alerts: AlertsService, private spinner: NgxSpinnerService) {
    router.events.subscribe((_: NavigationEnd) => this.currentUrl = _.url);
  }

  skill1 = new FormControl('', [Validators.required]);

  getErrorMessage() {
    return this.skill.hasError('required') ? 'Field is Required' :
           this.Manager.hasError('required') ? 'Field is Required' :
           this.project_name.hasError('required') ? 'Field is Required' :
           this.Senior_Manager.hasError('required') ? 'Field is Required' :
           '';
  }

  // tslint:disable-next-line:member-ordering
  matcher = new MyErrorStateMatcher();

  ngOnInit() {
    this.sub_role = localStorage.getItem('sub_role');
    if (this.sub_role === '6') {
      this.hr = true;
    } else {
      this.hr = false;
    }

    if (this.sub_role === '5') {
      this.manager = true;
    } else {
      this.manager = false;
    }

    this.addTeamDetails = [];
    this.spinner.show();
    this.data.getManagerList(this.token_id).subscribe(
      data => {
        this.managers = data['Manager list'];
      }
    );

    this.data.getSkills(this.token_id).subscribe(
      data => {
        this.skills = data['skill_details'];
      }
    );

    this.dataSource = new MatTableDataSource(this.addTeamDetails);

    this.data.getEmpSkillDetails(this.token_id).subscribe(

      data => {
        console.log(data);
        this.addTeamDetails = data['employee_details'];
        console.log(this.addTeamDetails);
        this.dataSource = new MatTableDataSource(this.addTeamDetails);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        setTimeout(() => {
          this.spinner.hide();
      });
      }
    );
  }


  onEdit(empid,onEdit) {
console.log(onEdit);
    this.server = this.type.find(x => x.emp_id === empid);
    this.type.push({
      emp_id: empid
    });
    this.abcd = this.type.includes(empid);
  }
  postProject(projectname, manager, seniormanager, skilldetail,date) {
    console.log(projectname);
    console.log(manager);
    console.log(date);
    // console.log(skilldetail);
    console.log(this.type);

    this.skilldetails = [];

    for (const i of skilldetail) {
      this.skilldetails.push({
        id: i['skill_id']
      });

    }
    console.log(this.skilldetails);
    this.spinner.show();
    this.data.postProjectDetails(projectname, manager, seniormanager, this.skilldetails, this.type, date, this.token_id).subscribe(

      data => {
        console.log(data);
        setTimeout(() => {
          this.spinner.hide();
      });
      });
  }
  postSkillId(skillid) {
    this.skilldetails = [];
    console.log(skillid);
    for (const i of skillid) {
      this.skilldetails.push({
        id: i['skill_id']
      });
    }
    this.spinner.show();
    console.log(this.skilldetails);
    this.data.postSkillId(this.skilldetails, this.token_id).subscribe(
      data => {
        this.addTeamDetails = data['employee_details'];
        console.log(this.addTeamDetails);
        this.dataSource = new MatTableDataSource(this.addTeamDetails);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        setTimeout(() => {
          this.spinner.hide();
      });
      });
  }
  
}

