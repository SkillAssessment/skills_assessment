import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { DataService } from '../data.service';
import { AlertsService } from 'angular-alert-module';

@Component({
  selector: 'app-main-menu',
  templateUrl: './main-menu.component.html',
  styleUrls: ['./main-menu.component.scss']
})
export class MainMenuComponent implements OnInit {

  status: any;
  id: any;
  url = localStorage.getItem('url');

  constructor(private data: DataService, private router: Router, public authService: AuthService, private alerts: AlertsService) { }
  // tslint:disable-next-line:use-life-cycle-interface
  ngOnInit() {
    this.id = localStorage.getItem('emp_id');
    console.log(this.router);

    console.log(this.url);
}

  logout(): void {
    console.log('Logout');
    this.authService.logout();
    this.router.navigate(['/login']);
    localStorage.clear();
    this.data.logoutCredentials(this.id).subscribe(
      data => {
        this.status = data['status'];
        this.alerts.setMessage(this.status, 'success');
      });
  }

}
