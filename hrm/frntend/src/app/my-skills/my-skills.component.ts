import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from '../data.service';
import { Router, NavigationEnd } from '@angular/router';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { AllSkills } from '../skill';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-my-skills',
  templateUrl: './my-skills.component.html',
  styleUrls: ['./my-skills.component.scss']
})
export class MySkillsComponent implements OnInit {

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  // tslint:disable-next-line:max-line-length
  constructor(private data: DataService, private router: Router, private spinner: NgxSpinnerService) {
    router.events.subscribe((_: NavigationEnd) => this.currentUrl = _.url);
  }

  displayedColumns: string[] = ['skill_name', 'evaluator_name', 'rating'];
  dataSource = new MatTableDataSource();
  id = localStorage.getItem('emp_id');
  token = localStorage.getItem('token');
  token_id = 'Token ' + this.token;

  All_Skills: AllSkills[] = [];
  currentUrl: string;

  ngOnInit() {
    this.All_Skills = [];
    this.dataSource = new MatTableDataSource(this.All_Skills);
    this.spinner.show();
    this.data.getAllSkills(this.id, this.token_id).subscribe(
      data => {
        // console.log(data);
        this.All_Skills = data['emp_details'];
        for (const i of this.All_Skills) {
          this.All_Skills = i['skills'];
        }
        console.log(this.All_Skills);
        this.dataSource = new MatTableDataSource(this.All_Skills);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        setTimeout(() => {
          this.spinner.hide();
      });
      }
    );
  }

}
