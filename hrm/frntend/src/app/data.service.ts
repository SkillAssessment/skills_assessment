import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class DataService {





  url = 'http://10.170.1.35:8000/api/';
  //url = 'http://192.168.43.189:8000/api/';




  userId: number;

  constructor(private http: HttpClient) { }

  // Login tab
  postCredentials(EmailID, Password) {
    return this.http.post(`${this.url}login/`, {
      email_id: EmailID,
      password: Password
    }
    );
  }

  // Personal edit tab 1
  editDetails(emp_id, emailid, Phonenumber, Altphonenumber, token) {
    const reqHeader = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': token });
    return this.http.post(`${this.url}editProfile/` + token, {
      'emp_id': emp_id,
      'email': emailid,
      'phone_number': Phonenumber,
      'alternative_phone_number': Altphonenumber
    }, { headers: reqHeader });
  }

  // Personal edit tab 2
  editDetails2(emp_id, Fathernumber, Mothernumber, Maritalstatus, Emergencynumber, Bloodgroup, token) {
    const reqHeader = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': token });
    return this.http.post(`${this.url}editProfile/` + token, {
      emp_id: emp_id,
      father_number: Fathernumber,
      mother_number: Mothernumber,
      marital_status: Maritalstatus,
      emergency_contact_number: Emergencynumber,
      blood_group: Bloodgroup
    }, { headers: reqHeader });
  }

  // Employee tabs
  getSkills(token) {
    const reqHeader = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': token });
    return this.http.get(`${this.url}skills/` + token, { headers: reqHeader });
  }

  getevaluator(id, token) {
    const reqHeader = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': token });
    return this.http.post(`${this.url}employeeManagers/` + token, { 'emp_id': id }, { headers: reqHeader });
  }

  postSkills(val, values, id, token) {
    const reqHeader = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': token });
    return this.http.post(`${this.url}addEmployeeSkill/` + token, {
      'details': {
        'emp_id': id, 'manager_id': values
      }, 'skills': val
    }, { headers: reqHeader });
  }

  getProjectsdetails(id, token) {
    const reqHeader = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': token });
    return this.http.post(`${this.url}projects/` + token, { 'emp_id': id }, { headers: reqHeader });
  }

  // Manager tabs
  get_empskills_details(id, token) {
    const reqHeader = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': token });
    return this.http.post(`${this.url}employeeSkillsEvaluation/` + token, { 'emp_id': id }, { headers: reqHeader });
  }

  postSkillRate(rate, manager_id, emp_id, token) {
    const reqHeader = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': token });
    return this.http.post(`${this.url}skillsRating/` + token, {
      'details': {
        'emp_id': emp_id, 'manager_id': manager_id
      }, 'skills': rate
    }, { headers: reqHeader });
  }

  get_emp_ratings(emp_id, token) {

    const reqHeader = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': token });
    return this.http.post(`${this.url}managerSkillsRatingList/` + token, { 'emp_id': emp_id }, { headers: reqHeader });

  }

  getManagerprojectsdetails(id, token) {

    const reqHeader = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': token });
    return this.http.post(`${this.url}projects/` + token, { 'emp_id': id }, { headers: reqHeader });

  }

  // Senior Management tabs
  getAllSkills(id, token) {

    const reqHeader = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': token });
    return this.http.post(`${this.url}employeeSkills/` + token, { 'emp_id': id }, { headers: reqHeader });
  }

  getAllskillRating(token) {

    const reqHeader = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': token });
    return this.http.post(`${this.url}skillsList/` + token, { 'skills': '' }, { headers: reqHeader });

  }

  newskill(skill, token) {

    const reqHeader = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': token });
    return this.http.post(`${this.url}addNewSkill/` + token, { 'skill': skill }, { headers: reqHeader });
  }

  newdesignation(designation, token) {

    const reqHeader = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': token });
    return this.http.post(`${this.url}createNewDesignation/` + token, { 'skill': designation }, { headers: reqHeader });
  }

  getallemployeespersonalinfo(token) {
    const reqHeader = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': token });
    return this.http.post(`${this.url}employeeList/` + token, { 'emp_id': '' }, { headers: reqHeader });
  }

  getAllprojectsdetails(token) {
    const reqHeader = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': token });
    return this.http.post(`${this.url}projectsList/` + token, { 'project_details': '' }, { headers: reqHeader });
  }

  // Logout tab
  logoutCredentials(id) {
    // const reqHeader = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': token });
    return this.http.post(`${this.url}logout/`, { 'emp_id': id });
  }

  serachSkillRate(emp_id, skill_id, rate, token) {
    const reqHeader = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': token });
    // tslint:disable-next-line:max-line-length
    return this.http.post(`${this.url}managerSkillsRatingList/` + token, { 'emp_id': emp_id, 'skill_id': skill_id, 'ratings': rate }, { headers: reqHeader });

  }

  postSkillid(id, token) {
    const reqHeader = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': token });
    return this.http.post(`${this.url}skillsList/` + token, {
      'skills': id
    }, { headers: reqHeader });
  }

  getProjects(token) {
    const reqHeader = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': token });
    return this.http.get(`${this.url}allProjects/` + token, { headers: reqHeader });
  }

  postProjectid(token, id) {
    const reqHeader = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': token });
    return this.http.post(`${this.url}projectsList/` + token, {
      'project_details': id
    }, { headers: reqHeader });
  }

  getDesignations(token) {
    const reqHeader = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': token });
    return this.http.get(`${this.url}designation/` + token, { headers: reqHeader });
  }

  // Personal edit tab 1
  // tslint:disable-next-line:max-line-length
  editEmployeeDetails(Employeename, EmployeeID, Status, Designation, Dateofpromotion, Dateofjoining, Monthsonjob, EmailIDpersonal, EmailID, Phonenumber, Alternativephonenumber, PANnumber, Aadharnumber, Fathername, Fathernumber, Mothername, Mothernumber, Emergencynumber, Gender, Dateofbirth, Permanentaddress, Residentialaddress, Qualification, Dateofresigning, Dateofrejoining, Bloodgroup, Maritalstatus, token) {
    const reqHeader = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': token });
    return this.http.post(`${this.url}hrEditProfile/` + token, {
      name: Employeename,
      emp_id: EmployeeID,
      status: Status,
      designation_id: Designation,
      date_of_promotion: Dateofpromotion,
      date_of_joining: Dateofjoining,
      months_on_job: Monthsonjob,
      email_id_personal: EmailIDpersonal,
      email: EmailID,
      phone_number: Phonenumber,
      alternative_phone_number: Alternativephonenumber,
      pan_number: PANnumber,
      aadhaar_number: Aadharnumber,
      father_name: Fathername,
      father_number: Fathernumber,
      mother_name: Mothername,
      mother_number: Mothernumber,
      emergency_contact_number: Emergencynumber,
      gender: Gender,
      date_of_birth: Dateofbirth,
      permanent_address: Permanentaddress,
      resident_address: Residentialaddress,
      qualification: Qualification,
      date_of_resigning: Dateofresigning,
      date_of_rejoining: Dateofrejoining,
      blood_group: Bloodgroup,
      marital_status: Maritalstatus
    }, { headers: reqHeader });
  }

  // tslint:disable-next-line:max-line-length
  confirmEditCreate(emp_id, name, gender, date_of_birth, email, email_id_personal, phone_number, alternative_phone_number, permanent_address, resident_address, blood_group, marital_status, pan_number, aadhaar_number, father_name, father_number, mother_name, mother_number, emergency_contact_number, designation, qualification, date_of_joining, months_on_job, date_of_resigning, date_of_rejoining, status, token) {
    const reqHeader = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': token });
    return this.http.post(`${this.url}hrEditProfile/` + token, {
      name: name,
      emp_id: emp_id,
      status: status,
      designation_id: designation,
      date_of_joining: date_of_joining,
      months_on_job: months_on_job,
      email_id_personal: email_id_personal,
      email: email,
      phone_number: phone_number,
      alternative_phone_number: alternative_phone_number,
      pan_number: pan_number,
      aadhaar_number: aadhaar_number,
      father_name: father_name,
      father_number: father_number,
      mother_name: mother_name,
      mother_number: mother_number,
      emergency_contact_number: emergency_contact_number,
      gender: gender,
      date_of_birth: date_of_birth,
      permanent_address: permanent_address,
      residential_address: resident_address,
      qualification: qualification,
      date_of_resigning: date_of_resigning,
      date_of_rejoining: date_of_rejoining,
      blood_group: blood_group,
      marital_status: marital_status
    }, { headers: reqHeader });
  }

  // Add Employee Tab
  // tslint:disable-next-line:max-line-length
  addEmployee(Employeename, Firstname, EmployeeID, Designation, Dateofjoining, EmailIDpersonal, EmailID, Phonenumber, Alternativephonenumber, PANnumber, Aadharnumber, Fathername, Fathernumber, Mothername, Mothernumber, Emergencynumber, Gender, Dateofbirth, Permanentaddress, Residentialaddress, Qualification, Bloodgroup, Worktimings, Maritalstatus, token) {
    const reqHeader = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': token });
    return this.http.post(`${this.url}createNewUser/` + token, {
      employee_name: Employeename,
      first_name: Firstname,
      emp_id: EmployeeID,
      designation_id: Designation,
      date_of_joining: Dateofjoining,
      email_id_personal: EmailIDpersonal,
      email: EmailID,
      phone_number: Phonenumber,
      alternative_phone_number: Alternativephonenumber,
      pan_number: PANnumber,
      aadhar_number: Aadharnumber,
      father_name: Fathername,
      father_number: Fathernumber,
      mother_name: Mothername,
      mother_number: Mothernumber,
      emergency_contact_number: Emergencynumber,
      gender: Gender,
      date_of_birth: Dateofbirth,
      permanent_address: Permanentaddress,
      residential_address: Residentialaddress,
      qualification: Qualification,
      blood_group: Bloodgroup,
      working_period_of_timings: Worktimings,
      marital_status: Maritalstatus
    }, { headers: reqHeader });
  }


  // Interviewer tab
  getinterviewr(token) {
    const reqHeader = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': token });
    return this.http.get(`${this.url}getInterviewers/` + token, { headers: reqHeader });
  }
  mail(token, emailid, subject) {
    const reqHeader = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': token });
    return this.http.post(`${this.url}interviewEmail/` + token, { 'email': emailid, 'message': subject }, { headers: reqHeader });
  }

  // tslint:disable-next-line:max-line-length
  postInterviewer_data(getname, getemailid, exp_data, phone_no, knowledge, verbal, type, notice_period, comments, attitude, recommandation, average, token) {
    const reqHeader = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': token });
    // tslint:disable-next-line:max-line-length
    return this.http.post(`${this.url}interview/` + token, { 'candidate_name': getname, 'email_id': getemailid, 'candidate_phone_number': phone_no, 'experience': exp_data, 'technical_knowledge': knowledge, 'verbal_communication_skills': verbal, 'type_of_employment': type, 'notice_period_time': notice_period, 'comments': comments, 'attitude': attitude, 'final_recommendation': recommandation, 'average_rating': average }, { headers: reqHeader });
  }

  intervieweeDetails(token) {
    const reqHeader = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': token });
    return this.http.get(`${this.url}intervieweesList/` + token, { headers: reqHeader });
  }

  // Change password
  changePassword(emp_id, oldpass, newpass, token) {
    const reqHeader = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': token });
    // tslint:disable-next-line:max-line-length
    return this.http.post(`${this.url}passwordChange/` + token, { 'emp_id': emp_id, 'old_password': oldpass, 'new_password': newpass }, { headers: reqHeader });
  }

  getManagerList(token) {
    const reqHeader = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': token });
    // tslint:disable-next-line:max-line-length
    return this.http.post(`${this.url}employeeCategory/` + token, { 'category': 'Manager' }, { headers: reqHeader });
  }
  getEmpSkillDetails(token) {
    const reqHeader = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': token });
    // tslint:disable-next-line:max-line-length
    return this.http.get(`${this.url}addEmployeeToProject/` + token, { headers: reqHeader });
  }
  postProjectDetails(projectname, manager, seniormanager, skilldetail, employess,date, token) {
    const reqHeader = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': token });
    // tslint:disable-next-line:max-line-length
    return this.http.post(`${this.url}addNewProject/` + token, { 'project_name': projectname, 'project_manager': manager, 'reporting_manager': seniormanager, 'employees': employess, 'skills': skilldetail,'started_on':date}, { headers: reqHeader });
  }
  postSkillId(id, token) {
    const reqHeader = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': token });
    // tslint:disable-next-line:max-line-length
    return this.http.post(`${this.url}searchAddEmployeeToProject/` + token, { 'skills': id }, { headers: reqHeader });
  }

  // Registration tab
  registrationDetails(name, email, phoneNumber, altPhoneNumber, address, fatherName, qualification, skills) {
    return this.http.post(`${this.url}interviewRegistration/`, {
      name: name,
      email_id: email,
      phone_number: phoneNumber,
      alternative_phone_number: altPhoneNumber,
      address: address,
      father_name: fatherName,
      qualification: qualification,
      skills: skills
    }
    );
  }
  postEmployeeid(empid, token) {
    const reqHeader = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': token });
    // tslint:disable-next-line:max-line-length
    return this.http.post(`${this.url}employeeList/` + token, { 'emp_id': empid }, { headers: reqHeader });
  }

}






