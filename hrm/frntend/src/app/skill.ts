export interface Skill {
  id: string;
}
export interface Projects {
  id: string;
}
export interface ManagerID {
  managerid: string;
}
export interface AllSkills {
  skill_name: string;
  evaluator_name: string;
  rating: string;
}
// tslint:disable-next-line:class-name
export interface projectDetails {
  project_name: string;
  manager_name: string;
  started_on: Date;
  project_status: string;
  completed_on: Date;
}

// tslint:disable-next-line:class-name
export interface empdata {
  empid: string;
  name: string;
}

// tslint:disable-next-line:class-name
export interface Skill_rate {
  skill_id: number;
  skill_rating: number;

}

export interface DialogData {
  animal: string;
  name1: string;
}

export interface addTeamDetails {
  select:string;
  name: string;
  emp_id: string;
  skill: string;
  project:string;
}

export interface EmployeeID
{
  emp_id:string;
}
