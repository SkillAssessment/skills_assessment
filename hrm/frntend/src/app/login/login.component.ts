import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { Router, NavigationEnd } from '@angular/router';
import { DataService } from '../data.service';
import { NgxSpinnerService } from 'ngx-spinner';

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    return !!(control && control.invalid && (control.dirty || control.touched));
  }
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {

  status: string;
  returnUrl: string;
  error: string;
  password_error: any;
  id_error: any;

  constructor(private data: DataService, private router: Router, private spinner: NgxSpinnerService) {
    router.events.subscribe((_: NavigationEnd) => this.currentUrl = _.url);
  }

  hide = true;

  email = new FormControl('', [Validators.required, Validators.email]);
  password = new FormControl('', [Validators.required, Validators.minLength(8)]);
  currentUrl: string;

  ngOnInit() {
    this.returnUrl = '/personal_details';

  }

  getErrorMessage() {
    return this.email.hasError('required') ? 'Invalid' :
           this.email.hasError('email') ? 'Invalid' :
           this.password.hasError('required') ? 'Invalid' :
           this.password.hasError('password') ? 'Invalid' :
            '';
  }

  // tslint:disable-next-line:member-ordering
  matcher = new MyErrorStateMatcher();

  postCredentials(EmailID, Password) {
    this.spinner.show();
    this.data.postCredentials(EmailID, Password).subscribe(
      data => {
        this.status = data['status'];
        this.password_error = data['password_error'];
        this.id_error = data['id_error'];
        setTimeout(() => {
          this.spinner.hide();
      });
        if (this.status === '0' && this.id_error === 'Enter valid Email_ID' ) {
          this.email.reset();
          this.password.reset();
          return;
        }
        if (this.status === '0' && this.password_error === 'Invalid Password') {
          this.password.reset();
          return;
        } else {
          const values = data['employee_details'];
          localStorage.setItem('token', data['token']);


          for (const i of values) {
            localStorage.setItem('emp_id', i['emp_id']);
            localStorage.setItem('name', i['name']);
            localStorage.setItem('designation', i['designation']);
            localStorage.setItem('gender', i['gender']);
            localStorage.setItem('date_of_birth', i['date_of_birth']);
            localStorage.setItem('father_name', i['father_name']);
            localStorage.setItem('father_number', i['father_number']);
            localStorage.setItem('mother_name', i['mother_name']);
            localStorage.setItem('mother_number', i['mother_number']);
            localStorage.setItem('permanent_address', i['permanent_address']);
            localStorage.setItem('resident_address', i['resident_address']);
            localStorage.setItem('marital_status', i['marital_status']);
            localStorage.setItem('email', i['email']);
            localStorage.setItem('email_id_personal', i['email_id_personal']);
            localStorage.setItem('phone_number', i['phone_number']);
            localStorage.setItem('alternative_phone_number', i['alternative_phone_number']);
            localStorage.setItem('pan_number', i['pan_number']);
            localStorage.setItem('aadhaar_number', i['aadhar_number']);
            localStorage.setItem('qualification', i['qualification']);
            localStorage.setItem('date_of_joining', i['date_of_joining']);
            localStorage.setItem('emergency_contact_number', i['emergency_contact_number']);
            localStorage.setItem('blood_group', i['blood_group']);
            localStorage.setItem('sub_role', i['sub_role']);
          }

          localStorage.setItem('isLoggedIn', 'true');
          this.router.navigate([this.returnUrl]);
        }
      });
  }
}

