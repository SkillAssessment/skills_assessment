import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { DataService } from '../data.service';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { NgxSpinnerService } from 'ngx-spinner';

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    return !!(control && control.invalid && (control.dirty || control.touched));
  }
}

@Component({
  selector: 'app-add-designation',
  templateUrl: './add-designation.component.html',
  styleUrls: ['./add-designation.component.scss']
})
export class AddDesignationComponent implements OnInit {

  token = localStorage.getItem('token');
  token_id = 'Token ' + this.token;
  status: any;
  currentUrl: any;

  constructor(private data: DataService, private router: Router, private spinner: NgxSpinnerService) {
    router.events.subscribe((_: NavigationEnd) => this.currentUrl = _.url);
  }

  designations = new FormControl('', [Validators.required]);

  getErrorMessage() {
    return this.designations.hasError('required') ? 'Field is Required' :
            '';
  }
  // tslint:disable-next-line:member-ordering
  matcher = new MyErrorStateMatcher();


  ngOnInit() {}
    newdesignation(designation) {
      this.spinner.show();
      this.data.newdesignation(designation, this.token_id).subscribe(
        data => {
          this.status = data['status'];
          setTimeout(() => {
            this.spinner.hide();
        });
        });
  }
  }


