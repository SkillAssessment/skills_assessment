import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { SkillsComponent } from './skills/skills.component';
import { MySkillsComponent } from './my-skills/my-skills.component';
import { ProjectDetailsComponent } from './project-details/project-details.component';
import { PersonalDetailsComponent } from './personal-details/personal-details.component';
import { SkillAssessmentComponent } from './skill-assessment/skill-assessment.component';
import { SkillRatingsComponent } from './skill-ratings/skill-ratings.component';
import { ManagerProjectDetailsComponent } from './manager-project-details/manager-project-details.component';
import { EmployeeCategoriesComponent } from './employee-categories/employee-categories.component';
import { AllProjectsInfoComponent } from './all-projects-info/all-projects-info.component';
import { AllSkillRatingsComponent } from './all-skill-ratings/all-skill-ratings.component';
import { InterviwerComponent } from './interviwer/interviwer.component';
import { InterviewResultsComponent } from './interview-results/interview-results.component';
import { MangerInterviwerComponent } from './manger-interviwer/manger-interviwer.component';
import { HomeComponent } from './home/home.component';
import { ChangepasswordComponent } from './changepassword/changepassword.component';
import { AddProjectComponent } from './add-project/add-project.component';
import { AddEmployeeComponent } from './add-employee/add-employee.component';
import { AddNewSkillComponent } from './add-new-skill/add-new-skill.component';
import { AddDesignationComponent } from './add-designation/add-designation.component';
import { PersonListComponent } from './person-list/person-list.component';

import { AuthGuard } from './auth.guard';


const routes: Routes = [
  {
    path: 'login',
  component: LoginComponent
  },
  {
    path: 'home',
    component: HomeComponent, canActivate: [AuthGuard]
  },
  {
    path: 'personal_details',
  component: PersonalDetailsComponent, canActivate: [AuthGuard]
  },
  {
    path: 'skills',
    component: SkillsComponent, canActivate: [AuthGuard]
  },
  {
    path: 'myskills',
    component: MySkillsComponent, canActivate: [AuthGuard]
  },
  {
    path: 'project_details',
    component: ProjectDetailsComponent, canActivate: [AuthGuard]
  },
  {
    path: 'skill_assess',
    component: SkillAssessmentComponent, canActivate: [AuthGuard]
  },
  {
    path: 'skill_ratings',
    component: SkillRatingsComponent, canActivate: [AuthGuard]
  },
  {
    path: 'manager_project_details',
    component: ManagerProjectDetailsComponent, canActivate: [AuthGuard]
  },
  {
    path: 'employee_categories',
    component: EmployeeCategoriesComponent, canActivate: [AuthGuard]
  },
  {
    path: 'all_skills',
    component: AllSkillRatingsComponent, canActivate: [AuthGuard]
  },
  {
    path: 'all_projects',
    component: AllProjectsInfoComponent, canActivate: [AuthGuard]
  },
  {
    path: 'interviewer',
    component: InterviwerComponent, canActivate: [AuthGuard]
  },
  {
    path: 'interviewresults',
    component: InterviewResultsComponent, canActivate: [AuthGuard]
  },
  {
    path: 'mangerInterviwer',
    component: MangerInterviwerComponent, canActivate: [AuthGuard]
  },
  {
    path: 'changepassword',
  component: ChangepasswordComponent, canActivate: [AuthGuard]
  },
  {
    path: 'addproject',
  component: AddProjectComponent, canActivate: [AuthGuard]
  },
  {
    path: 'add_employee',
  component: AddEmployeeComponent, canActivate: [AuthGuard]
  },
  {
    path: 'add_new_skill',
  component: AddNewSkillComponent, canActivate: [AuthGuard]
  },
  {
    path: 'add_new_designation',
  component: AddDesignationComponent, canActivate: [AuthGuard]
  },
  {
    path: 'person_list',
  component: PersonListComponent, canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
