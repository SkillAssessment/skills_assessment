import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { DataService } from '../data.service';
import { NavigationEnd, Router } from '@angular/router';
import { AlertsService } from 'angular-alert-module';
import { MyErrorStateMatcher } from 'src/app/default.error-matcher';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-interviwer',
  templateUrl: './interviwer.component.html',
  styleUrls: ['./interviwer.component.scss']
})
export class InterviwerComponent implements OnInit {
  currentUrl: string;
  interviwers: object;

  interviwers1 = new FormControl('', [Validators.required]);
  message = new FormControl('', [Validators.required]);
  id = localStorage.getItem('emp_id');
  token = localStorage.getItem('token');
  token_id = 'Token ' + this.token;
  status: any;

  constructor(private data: DataService, private router: Router, private alerts: AlertsService, private spinner: NgxSpinnerService) {
    router.events.subscribe((_: NavigationEnd) => this.currentUrl = _.url);
  }
  matcher = new MyErrorStateMatcher();

  getErrorMessage() {
    return this.interviwers1.hasError('required') ? 'Field is Required' :
           this.message.hasError('required') ? 'Field is Required' :
    '';
  }

  ngOnInit() {
    this.spinner.show();
  console.log(this.token_id);
    this.data.getinterviewr(this.token_id).subscribe(
      data => {
        this.interviwers = data['interviewers_list'];
        setTimeout(() => {
          this.spinner.hide();
      });
        console.log(this.interviwers);
      }
    );
  }
  mail(email, subject) {
    this.spinner.show();
    console.log(email);
    console.log(subject);
    this.data.mail(this.token_id, email, subject).subscribe(
      data => {
        console.log(data);
        this.status = data['status'];
        setTimeout(() => {
          this.spinner.hide();
      });
      }
    );
  }
}


