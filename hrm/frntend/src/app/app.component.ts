import { Component, OnInit } from '@angular/core';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';
import { DataService } from './data.service';
import { AlertsService } from 'angular-alert-module';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {

  constructor(private data: DataService, private router: Router, public authService: AuthService, private alerts: AlertsService) { }

  ngOnInit() {
  }
}
