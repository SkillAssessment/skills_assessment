import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from '../data.service';
import { Router, NavigationEnd } from '@angular/router';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { NgxSpinnerService } from 'ngx-spinner';

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    return !!(control && control.invalid && (control.dirty || control.touched));
  }
}

@Component({
  selector: 'app-employee-categories',
  templateUrl: './employee-categories.component.html',
  styleUrls: ['./employee-categories.component.scss']
})
export class EmployeeCategoriesComponent implements OnInit {

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  emp_id = localStorage.getItem('emp_id');

  designations: object;
  designation1 = new FormControl();

  currentUrl: string;
  personal_details = [];
  token = localStorage.getItem('token');
  token_id = 'Token ' + this.token;
  profile_status: string;

  constructor(private data: DataService, private router: Router, private spinner: NgxSpinnerService) {
    router.events.subscribe((_: NavigationEnd) => this.currentUrl = _.url);
  }

  displayedColumns: string[] =
    // tslint:disable-next-line:max-line-length
    ['name', 'emp_id', 'status', 'designation', 'date_of_promotion', 'date_of_joining', 'months_on_job', 'phone_number', 'alternative_phone_number', 'email', 'email_id_personal', 'pan_number', 'aadhar_number', 'father_name', 'father_number', 'mother_name', 'mother_number', 'emergency_contact_number', 'gender', 'date_of_birth', 'permanent_address', 'resident_address', 'qualification', 'date_of_resigning', 'date_of_rejoining', 'blood_group', 'marital_status'];
  dataSource = new MatTableDataSource();

  emailid = new FormControl('', [Validators.email]);
  Phonenumber = new FormControl('', [Validators.minLength(10)]);
  Altphonenumber = new FormControl('', [Validators.minLength(10)]);
  Fathernumber = new FormControl('', [Validators.minLength(10)]);
  Mothernumber = new FormControl('', [Validators.minLength(10)]);
  Maritalstatus = new FormControl('', [Validators.email]);
  Emergencynumber = new FormControl('', [Validators.email]);
  Bloodgroup = new FormControl('', [Validators.email]);

  emp_id1: string;
  emp_name: string;
  status: string;
  designation: string;
  date_of_promotion: string;
  date_of_joining: string;
  months_on_job: string;
  emailid_personal: string;
  email_office: string;
  phone_no: string;
  alt_phone_no: string;
  pan_no: string;
  aadhar_no: string;
  father_name: string;
  father_no: string;
  mother_name: string;
  mother_no: string;
  emergency_contact_no: string;
  gender: string;
  permanent_address: string;
  resident_address: string;
  qualification: string;
  marital_status: string;
  blood_group: string;
  date_of_resigning: string;
  date_of_rejoining: string;
  date_of_birth: string;

  ngOnInit() {
    this.spinner.show();
    this.data.getDesignations(this.token_id).subscribe(
      data => {
        console.log(data);
        this.designations = data['designations'];
      }
    );

    this.data.getallemployeespersonalinfo(this.token_id).subscribe(
      data => {
        console.log(data);
        this.personal_details = data['emp_details'];
        this.dataSource = data['emp_details'];
        this.dataSource = new MatTableDataSource(this.personal_details);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        setTimeout(() => {
          this.spinner.hide();
        });
        console.log('data');
      }
    );
  }

  getErrorMessage() {
    //   return this.emailid.hasError('email') ? 'Invalid' :
    //       this.Phonenumber.hasError('Phonenumber') ? 'Invalid' :
    //       this.Altphonenumber.hasError('Altphonenumber') ? 'Invalid' :
    //           '';
  }
  // tslint:disable-next-line:member-ordering
  matcher = new MyErrorStateMatcher();


  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  // tslint:disable-next-line:max-line-length
  editEmployeeDetails(Employeename, EmployeeID, Status, Designation, Dateofpromotion, Dateofjoining, Monthsonjob, EmailIDpersonal, EmailID, Phonenumber, Alternativephonenumber, PANnumber, Aadharnumber, Fathername, Fathernumber, Mothername, Mothernumber, Emergencynumber, Gender, Dateofbirth, Permanentaddress, Residentialaddress, Qualification, Dateofresigning, Dateofrejoining, Bloodgroup, Maritalstatus) {
    console.log(Mothername);
    console.log(Dateofbirth);
    this.spinner.show();
    // tslint:disable-next-line:max-line-length
    this.data.editEmployeeDetails(Employeename, EmployeeID, Status, Designation, Dateofpromotion, Dateofjoining, Monthsonjob, EmailIDpersonal, EmailID, Phonenumber, Alternativephonenumber, PANnumber, Aadharnumber, Fathername, Fathernumber, Mothername, Mothernumber, Emergencynumber, Gender, Dateofbirth, Permanentaddress, Residentialaddress, Qualification, Dateofresigning, Dateofrejoining, Bloodgroup, Maritalstatus, this.token_id).subscribe(
      data => {
        this.profile_status = data['profile_status'];
         setTimeout(() => {
          this.spinner.hide();
        });
      });
  }
  postEmployeeid(empid) {
    this.data.postEmployeeid(empid, this.token_id).subscribe(
      data => {
        this.emp_id1 = data['emp_details'][0]['emp_id'];
        this.emp_name = data['emp_details'][0]['name'];
        this.status = data['emp_details'][0]['status'];
        this.aadhar_no = data['emp_details'][0]['aadhar_number'];
        this.designation = data['emp_details'][0]['designation'];
        this.date_of_promotion = data['emp_details'][0]['date_of_promotion'];
        this.date_of_joining = data['emp_details'][0]['date_of_joining'];
        this.months_on_job = data['emp_details'][0]['months_on_job'];
        this.emailid_personal = data['emp_details'][0]['email_id_personal'];
        this.email_office = data['emp_details'][0]['email'];
        this.phone_no = data['emp_details'][0]['phone_number'];
        this.alt_phone_no = data['emp_details'][0]['alternative_phone_number'];
        this.pan_no = data['emp_details'][0]['pan_number'];
        this.aadhar_no = data['emp_details'][0]['aadhar_number'];
        this.father_name = data['emp_details'][0]['father_name'];
        this.father_no = data['emp_details'][0]['father_number'];
        this.mother_name = data['emp_details'][0]['mother_name'];
        this.mother_no = data['emp_details'][0]['mother_number'];
        this.emergency_contact_no = data['emp_details'][0]['emergency_contact_number'];
        this.gender = data['emp_details'][0]['gender'];
        this.permanent_address = data['emp_details'][0]['permanent_address'];
        this.resident_address = data['emp_details'][0]['resident_address'];
        this.qualification = data['emp_details'][0]['qualification'];
        this.marital_status = data['emp_details'][0]['marital_status'];
        this.blood_group = data['emp_details'][0]['blood_group'];
        this.date_of_resigning = data['emp_details'][0]['date_of_resigning'];
        this.date_of_rejoining = data['emp_details'][0]['date_of_rejoining'];
        this.date_of_birth = data['emp_details'][0]['date_of_birth'];
      });
  }

}
