from rest_framework_json_api import serializers
from .models import EmployeeDetails, Skills, ProjectsDetails, SkillEvaluation, Registration, EmployeeProjects, \
    Interview, Designation


class EmployeeSerializer(serializers.ModelSerializer):
    class Meta:
        model = EmployeeDetails
        exclude = ('password', 'last_login', 'is_active', 'is_staff', 'groups', 'user_permissions', 'is_superuser')


class ProjectEmployeeSerializer(serializers.ModelSerializer):
    emp_id = serializers.CharField(source='emp_id.emp_id')
    project = serializers.CharField(source='project.project_id')

    class Meta:
        model = EmployeeProjects
        fields = ('emp_id', 'project_contribution', 'project')


class SkillSerializer(serializers.ModelSerializer):
    class Meta:
        model = Skills
        fields = '__all__'


class DesignationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Designation
        fields = '__all__'


class ProjectSerializer(serializers.ModelSerializer):
    project_manager_id = serializers.CharField(source='project_manager.emp_id')

    class Meta:
        model = ProjectsDetails
        fields = ('project_name', 'project_status', 'started_on', 'completed_on', 'project_manager_id')


class EmployeeProjectSerializer(serializers.ModelSerializer):

    class Meta:
        model = ProjectsDetails
        fields = ('project_id', 'project_name', 'project_status', 'started_on', 'completed_on', 'project_manager_id',
                  'primary_cost_center', 'secondary_cost_center', 'project_status')


class ManagerSerializer(serializers.ModelSerializer):
    manager_id = serializers.CharField(source='emp_id')
    manager_name = serializers.CharField(source='name')

    class Meta:
        model = EmployeeDetails
        fields = ('manager_name', 'manager_id')


class EmployeeSkillsSerializer(serializers.ModelSerializer):
    class Meta:
        model = EmployeeDetails
        fields = ('name', 'emp_id')


class RegistrationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Registration
        fields = "__all__"


class InterviewSerializer(serializers.ModelSerializer):
    class Meta:
        model = Interview
        fields = "__all__"


class SkillEvaluationSerializer(serializers.ModelSerializer):
    evaluator_id = serializers.CharField(source='evaluator_id.emp_id')

    class Meta:
        model = SkillEvaluation
        fields = ('evaluator_id', 'rating', 'skill_id')


class ManagerSkillRatingsSerializer(serializers.ModelSerializer):
    emp_id = serializers.CharField(source='emp_id.emp_id')

    class Meta:
        model = SkillEvaluation
        fields = ('skill_id', 'rating', 'evaluated_at', 'emp_id')
