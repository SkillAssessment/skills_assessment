from django.db import models
from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.models import BaseUserManager


class UserProfileManager(BaseUserManager):
    """Help django works with our custom user model"""
    def create_user(self, email, name, password=None):
        if not email:
            raise ValueError('User must have an email address.')
        email = self.normalize_email(email)
        user = self.model(email=email, name=name)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, name, password):
        """Create and save a new superuser with given details"""
        user = self.create_user(email, name, password)
        user.is_superuser = True
        user.is_staff = True
        user.save(using=self._db)
        return user


class EmployeeDetails(AbstractBaseUser, PermissionsMixin):
    """ Represents a user profile inside our system"""
    emp_id = models.CharField(db_column='Emp_Id', primary_key=True, max_length=20)
    name = models.CharField(db_column='Emp_Name', max_length=255, blank=True, null=True)
    first_name = models.CharField(db_column='First_Name', max_length=255, blank=True, null=True)
    qualification = models.CharField(db_column='Qualification', max_length=255, blank=True, null=True)
    date_of_joining = models.DateField(db_column='Date_Of_Joining', blank=True, null=True)
    months_on_job = models.FloatField(db_column='Months_On_Job', blank=True, null=True)
    date_of_resigning = models.DateField(db_column='Date_Of_Resigning', blank=True, null=True)
    date_of_rejoining = models.DateField(db_column='Date_Of_Rejoining', blank=True, null=True)
    gender = models.CharField(db_column='Gender', max_length=10, blank=True, null=True)
    date_of_birth = models.DateField(db_column='Date_Of_Birth', blank=True, null=True)
    permanent_address = models.CharField(db_column='Permanent_Address', max_length=255, blank=True, null=True)
    resident_address = models.CharField(db_column='Residential_Address', max_length=255, blank=True, null=True)
    blood_group = models.CharField(db_column='Blood_Group', max_length=10, blank=True, null=True)
    pan_number = models.CharField(db_column='PAN_Number', max_length=10, blank=True, null=True)
    aadhar_number = models.CharField(db_column='Aadhar_Number', max_length=16, blank=True, null=True)
    alternative_phone_number = models.CharField(max_length=30, db_column='Alternative_Phone_Number', blank=True,
                                                null=True)
    phone_number = models.CharField(db_column='Phone_Number', max_length=30, blank=True, null=True)
    father_name = models.CharField(db_column='Father_Name', max_length=255, blank=True, null=True)
    father_number = models.CharField(db_column='Father_Number', max_length=30, blank=True, null=True)
    mother_name = models.CharField(db_column='Mother_Name', max_length=255, blank=True, null=True)
    mother_number = models.CharField(db_column='Mother_Number', max_length=30, blank=True, null=True)
    emergency_contact_number = models.CharField(db_column='Emergency_Contact_Number', max_length=30, blank=True,
                                                null=True)
    email_id_personal = models.CharField(db_column='Email_Id_Personal', max_length=255, blank=True, null=True)
    email = models.EmailField(db_column='Email_Id_Office', max_length=255, unique=True, blank=True, null=True)
    password = models.CharField(db_column='Password', max_length=100, blank=True, null=True)
    marital_status = models.CharField(db_column='Marital_Status', max_length=40, blank=True, null=True)
    document = models.CharField(db_column='Document', max_length=50, blank=True, null=True)
    profile_picture = models.TextField(db_column='Profile_Picture', blank=True, null=True)
    status = models.CharField(db_column='Status', blank=True, null=True, max_length=50)
    working_period_of_timings = models.CharField(db_column='Working_Period_Of_Timing', max_length=225, blank=True,
                                                 null=True)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    last_login = models.DateTimeField(db_column='Last_Login', blank=True, null=True)

    objects = UserProfileManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['name']

    def __str__(self):
        """django uses this when it needs to convert the object to a string"""
        return self.email


class MainRoles(models.Model):
    main_role_id = models.AutoField(db_column='Main_Role_id', primary_key=True)
    roles = models.CharField(db_column='Roles', max_length=60, blank=True, null=True)
    objects = UserProfileManager()


class SubRoles(models.Model):
    sub_role_id = models.AutoField(db_column='Sub_Role_Id', primary_key=True)
    main_role = models.ForeignKey(MainRoles, on_delete=models.DO_NOTHING, db_column='Main_Role_id')
    sub_roles = models.CharField(db_column='Sub_Roles', max_length=60, blank=True, null=True)
    objects = UserProfileManager()


class RoleAssignment(models.Model):
    role_assignment_id = models.AutoField(db_column='Role_Assignment_Id', primary_key=True)
    sub_role = models.ForeignKey(SubRoles, on_delete=models.DO_NOTHING, db_column='Sub_Role_Id', blank=True, null=True)
    emp_id = models.ForeignKey(EmployeeDetails, on_delete=models.DO_NOTHING, db_column='Emp_Id', blank=True, null=True)
    objects = UserProfileManager()


class EmployeeManagers(models.Model):
    employee_manager_id = models.AutoField(db_column='Employee_Manager_Id', primary_key=True)
    emp_id = models.ForeignKey(EmployeeDetails, on_delete=models.DO_NOTHING, db_column='Emp_Id', blank=True, null=True)
    manager = models.ForeignKey(EmployeeDetails, on_delete=models.DO_NOTHING, db_column='Manager_Id', blank=True,
                                null=True, related_name='Emp_id3')
    objects = UserProfileManager()


class EmployeeProjects(models.Model):
    """Represents all the projects a employee is currently working in"""
    employee_project_id = models.AutoField(db_column='Employee_Project_Id', primary_key=True)
    project = models.ForeignKey('ProjectsDetails', on_delete=models.DO_NOTHING, db_column='Project_Id', blank=True,
                                null=True)
    emp_id = models.ForeignKey(EmployeeDetails, on_delete=models.DO_NOTHING, db_column='Employee_Id',
                               related_name='Emp_id', blank=True, null=True)
    project_manager_id = models.ForeignKey(EmployeeDetails, models.DO_NOTHING, db_column='Project_Manager_Id',
                                           related_name='Manger_id', blank=True, null=True)
    project_contribution = models.IntegerField(db_column='Project_Contribution', blank=True, null=True)
    objects = UserProfileManager()


class ProjectSkills(models.Model):
    project_skills_id = models.IntegerField(db_column='Project_Skills_Id', primary_key=True)
    skill = models.ForeignKey('Skills', on_delete=models.DO_NOTHING, db_column='Skill_Id', blank=True, null=True)
    project = models.ForeignKey('ProjectsDetails', on_delete=models.DO_NOTHING, db_column='Project_Id', blank=True,
                                null=True)
    objects = UserProfileManager()


class ProjectsDetails(models.Model):
    project_id = models.AutoField(db_column='Project_Id', primary_key=True)
    project_name = models.CharField(db_column='Project_Name', max_length=45, blank=True, null=True)
    project_manager = models.ForeignKey(EmployeeDetails, on_delete=models.DO_NOTHING, db_column='Project_Manager',
                                        blank=True, null=True)
    reporting_manager_id = models.ForeignKey(EmployeeDetails, on_delete=models.DO_NOTHING,
                                             db_column='Reporting_Manager_id', blank=True, null=True,
                                             related_name='Reporting_manager')
    started_on = models.DateField(db_column='Started_On', blank=True, null=True)
    completed_on = models.DateField(db_column='Completed_On', blank=True, null=True)
    primary_cost_center = models.CharField(db_column='Primary_Cost_Center', max_length=100, blank=True, null=True)
    secondary_cost_center = models.CharField(db_column='Secondary_Cost_Center', max_length=100, blank=True, null=True)
    project_status = models.CharField(db_column='Project_Status', max_length=45, blank=True, null=True)
    objects = UserProfileManager()


class Designation(models.Model):
    designation_id = models.AutoField(db_column='Designation_Id', primary_key=True)
    designation = models.CharField(db_column='Designation', max_length=45, blank=True, null=True)
    objects = UserProfileManager()


class Promotion(models.Model):
    promotion_id = models.AutoField(db_column='Promotion_Id', primary_key=True)
    date_of_promotion = models.CharField(db_column='Date_Of_Promotion', max_length=30, blank=True, null=True)
    emp_id = models.ForeignKey(EmployeeDetails, on_delete=models.DO_NOTHING, db_column='Emp_Id', blank=True, null=True)
    designation_id = models.ForeignKey(Designation, models.DO_NOTHING, db_column='Designation_Id', blank=True,
                                       null=True)
    objects = UserProfileManager()


class Registration(models.Model):
    employee_registration_id = models.AutoField(db_column='Employee_Registration_Id', primary_key=True, max_length=45)
    name = models.CharField(db_column='Name', max_length=255, blank=True, null=True)
    email_id = models.CharField(db_column='Email_id', max_length=45, blank=True, null=True)
    phone_number = models.BigIntegerField(db_column='Phone_Number', blank=True, null=True)
    alternative_phone_number = models.BigIntegerField(db_column='Alternative_Phone_Number', blank=True, null=True)
    gender = models.CharField(db_column='Gender', max_length=10, blank=True, null=True)
    date_of_birth = models.DateField(db_column='Date_Of_Birth', blank=True, null=True)
    address = models.CharField(db_column='Address', max_length=255, blank=True, null=True)
    father_name = models.CharField(db_column='Father_Name', max_length=255, blank=True, null=True)
    qualification = models.CharField(db_column='Qualification', max_length=45, blank=True, null=True)
    skills = models.CharField(db_column='Skills', max_length=45, blank=True, null=True)
    experience_in = models.CharField(db_column='Experience', max_length=10, blank=True, null=True)
    interested_field = models.CharField(db_column='Interested_Field', max_length=45, blank=True, null=True)
    interviewer_id = models.CharField(db_column='Interviewer_Id', max_length=45, blank=True, null=True)
    technical_round_result = models.CharField(db_column='Technical_Round_Result', max_length=10, blank=True, null=True)
    final_result = models.CharField(db_column='Final_Result', max_length=45, blank=True, null=True)
    objects = UserProfileManager()


class SkillEvaluation(models.Model):
    skill_evaluation_id = models.AutoField(db_column='Skill_Evaluation_Id', primary_key=True)
    skill = models.ForeignKey('Skills', on_delete=models.DO_NOTHING, db_column='Skill_Id', blank=True, null=True)
    rating = models.IntegerField(db_column='Rating', blank=True, null=True)
    re_evaluate = models.BooleanField(db_column='Re_Evaluate', blank=True, default=True)
    evaluator_id = models.ForeignKey(EmployeeDetails, on_delete=models.DO_NOTHING, db_column='Evaluator_id',
                                     max_length=20, related_name='Emp_id2', blank=True, null=True)
    emp_id = models.ForeignKey(EmployeeDetails, on_delete=models.DO_NOTHING, db_column='Emp_Id', max_length=20,
                               blank=True, null=True)
    created_at = models.DateTimeField(db_column='Created_at', blank=True, null=True)
    evaluated_at = models.DateTimeField(db_column='Evaluated_at', blank=True, null=True)
    objects = UserProfileManager()

    class Meta:
        get_latest_by = 'rating'


class Skills(models.Model):
    skill_id = models.AutoField(db_column='Skill_Id', primary_key=True)
    skill_name = models.CharField(db_column='Skill_Name', max_length=45, blank=True, null=True)
    objects = UserProfileManager()


class Interview(models.Model):
    interview_id = models.AutoField(db_column='Interview_Id', primary_key=True)
    candidate_name = models.CharField(db_column='Candidate_Name', max_length=225, blank=False, null=False)
    candidate_phone_number = models.BigIntegerField(db_column='Candidate_Phone_Number', blank=True, null=True)
    email_id = models.CharField(db_column='Email_Id_Of_Candidate', max_length=225, blank=False, null=False)
    experience = models.IntegerField(db_column='Years_Of_Relevant_Experience', blank=False)
    technical_knowledge = models.CharField(db_column='Technical_Knowledge', max_length=45, blank=False, null=True)
    verbal_communication_skills = models.CharField(db_column='Verbal_Communication_Skills', max_length=45, blank=True,
                                                   null=True)
    type_of_employment = models.CharField(db_column='Type_Of_Employment_At_Ambertag', max_length=45, blank=True,
                                          null=True)
    notice_period_time = models.CharField(db_column='Notice_Period_From_Prior_company', max_length=45, blank=True,
                                          null=True)
    comments = models.CharField(db_column='Additional_Comments', max_length=225, blank=True, null=True)
    attitude = models.CharField(db_column='Attitude_of_candidate', max_length=45, blank=True, null=True)
    final_recommendation = models.CharField(db_column='Final_Recommendation', max_length=45, blank=True, null=True)
    average_rating = models.IntegerField(db_column='Average_rating', blank=True, null=True)
    interview_date = models.DateField(db_column='Interview_Date', blank=True, null=True)
    objects = UserProfileManager()


class Password(models.Model):
    id = models.IntegerField(primary_key=True)
    email = models.CharField(max_length=45, blank=True, null=True)
    emp_id = models.CharField(max_length=20, blank=True, null=True)
    password = models.CharField(db_column='password', max_length=45, blank=True, null=True)
