from .models import EmployeeDetails, RoleAssignment, SubRoles, Skills, SkillEvaluation, EmployeeProjects, \
    ProjectsDetails, EmployeeManagers, Registration, Promotion, Designation, Interview, Password, ProjectSkills
from rest_framework.response import Response
from rest_framework.views import APIView
from .serializers import EmployeeSerializer, SkillSerializer, ProjectSerializer, ManagerSerializer, \
    EmployeeSkillsSerializer, RegistrationSerializer, ProjectEmployeeSerializer, SkillEvaluationSerializer, \
    EmployeeProjectSerializer, ManagerSkillRatingsSerializer, InterviewSerializer, DesignationSerializer
from django.contrib.auth import authenticate
from django.contrib.auth.hashers import check_password
from rest_framework.authtoken.models import Token
from rest_framework.permissions import AllowAny
from rest_framework.decorators import permission_classes
from django.utils import timezone
from django_auditor.auditor import Audit
from django.core.mail import send_mail
from django.contrib.auth.base_user import BaseUserManager
import datetime
from django.utils.dateparse import parse_date


@permission_classes((AllowAny,))
class Login(APIView):
    """Used to authenticate a user and log them into the system"""
    @staticmethod
    def post(request):
        email = request.data.get('email_id', None)
        password = request.data.get('password', None)
        if EmployeeDetails.objects.filter(email=email).exists() is True:
            user = authenticate(username=email, password=password)
            if user:
                emp_id = EmployeeDetails.objects.filter(email=email).values_list('emp_id', flat=True)[0]
                if Token.objects.filter(user_id=emp_id).exists() is True:
                    Token.objects.filter(user_id=emp_id).delete()
                details = EmployeeDetails.objects.filter(email=email)
                details = EmployeeSerializer(details, many=True)
                EmployeeDetails.objects.filter(email=email).update(last_login=datetime.datetime.now())
                data = details.data[:]
                sub_role = RoleAssignment.objects.filter(emp_id=data[0]['emp_id']).values_list('sub_role', flat=True)[0]
                data[0]['sub_role'] = sub_role
                main_role1 = SubRoles.objects.filter(sub_role_id=sub_role).values_list('main_role_id', flat=True)[0]
                data[0]['main_role'] = main_role1
                designation_id = Promotion.objects.filter(emp_id=data[0]['emp_id']).\
                    values_list('designation_id', flat=True).latest('date_of_promotion')
                designation_name = Designation.objects.filter(designation_id=designation_id). \
                    values_list('designation', flat=True)[0]
                data[0]['designation'] = designation_name
                token, _ = Token.objects.get_or_create(user=user)
                return Response({"status": "1", "token": token.key, "employee_details": data})
            return Response({'status': "0", "password_error": "Invalid Password"})
        return Response({'status': '0', 'id_error': "Enter valid Email_ID"})


class Skill(APIView):
    """Gives all the skills which are currently used in the company"""
    @staticmethod
    def get(_):
        skill = Skills.objects.all()
        skill = SkillSerializer(skill, many=True)
        return Response({"skill_details": skill.data})


class AllProjects(APIView):
    @staticmethod
    def get(_):
        projects = ProjectsDetails.objects.all()
        projects = EmployeeProjectSerializer(projects, many=True)
        return Response({"project_details": projects.data})


class AllDesignation(APIView):
    """Used to display all the designation in the company"""
    @staticmethod
    def get(_):
        designations = Designation.objects.all()
        designations = DesignationSerializer(designations, many=True)
        return Response({"designations": designations.data})


class AddEmployeeSkill(APIView):
    """Used by employees to add new skills"""
    @staticmethod
    def post(request):
        emp_id = request.data.get('details', dict()).get('emp_id')
        manager_id = request.data.get('details', dict()).get('manager_id')
        skills = request.data.get('skills', None)
        list1 = []
        status = {}
        status1 = {}
        string = ""
        if emp_id and manager_id and skills:
            for skill_id in skills:
                for value in skill_id.values():
                    if SkillEvaluation.objects.filter(emp_id=emp_id, skill_id=value, evaluator_id=manager_id).exists() \
                            is False:
                        p1 = SkillEvaluation(emp_id_id=emp_id, skill_id=value, evaluator_id_id=manager_id,
                                             created_at=timezone.now().today())
                        p1.save()
                        Audit(request, p1).create()
                        skill = Skills.objects.filter(skill_id=value)
                        skill = SkillSerializer(skill, many=True)
                        for data1 in skill.data:
                            manager_name = EmployeeDetails.objects.filter(
                                emp_id=request.data.get('details', dict()).get('manager_id')).values_list('name',
                                                                                                          flat=True)[0]
                            data1['manager_name'] = manager_name
                            data1['manger_id'] = manager_id
                            list1.append(data1)
                        status = {"status": 'Skill Added', 'skill_details': list1}
                    else:
                        skill_name = Skills.objects.filter(skill_id=value).values_list('skill_name', flat=True)[0]
                        string += skill_name + " "
                        status1 = {"status": string + "Already Added", "skill_details": list1}
            if string:
                return Response(status1)
            return Response(status)
        return Response({"error": "Please enter all the required fields"})


class EmployeeManager(APIView):
    """Gives the manager list of an employee"""
    @staticmethod
    def post(request):
        list1 = []
        emp_id = request.data.get('emp_id', None)
        if emp_id:
            manager_id = EmployeeManagers.objects.filter(emp_id=emp_id).values_list('manager_id', flat=True)
            if manager_id:
                for manager_id in manager_id:
                    name = ManagerSerializer(EmployeeDetails.objects.filter(emp_id=manager_id), many=True)
                    list1.append(name.data[0])
                return Response({"manager_details": list1})
            return Response({"status": "No Mangers Assigned"})
        return Response({"status": "Please enter all the required fields"})


class EmployeeSkillsEvaluation(APIView):
    """Used to send a list of skills of an employee to there respective managers for evaluation"""
    @staticmethod
    def post(request):
        list1 = []
        list2 = []
        emp_id = request.data.get('emp_id', None)
        if emp_id:
            if SkillEvaluation.objects.filter(evaluator_id=emp_id, rating__isnull=True).exists() is True:
                emp_list = SkillEvaluation.objects.filter(evaluator_id=emp_id, rating__isnull=True). \
                    values_list('emp_id').distinct()
                for emp_details in emp_list:
                    emp_details = EmployeeDetails.objects.filter(emp_id=emp_details[0])
                    emp_details = EmployeeSkillsSerializer(emp_details, many=True)
                    for emp_list in emp_details.data:
                        skills = SkillEvaluation.objects.filter(emp_id=emp_list['emp_id'], evaluator_id=emp_id,
                                                                rating__isnull=True).values_list('skill_id')
                        for skills in skills:
                            skill_details = Skills.objects.filter(skill_id=skills[0]).values('skill_id', 'skill_name')
                            list1.append(skill_details[0])
                            emp_list['skill_details'] = list1
                        list1 = []
                    list2.append(emp_list)
                return Response({"emp_details": list2})
            else:
                return Response({"error": "All Evaluations Completed"})
        return Response({"status": "Please enter all the required fields"})


class SkillRating(APIView):
    """Used to evaluate employee skills"""
    @staticmethod
    def post(request):
        emp_id = request.data.get('details', dict()).get('emp_id')
        skills = request.data.get('skills', None)
        list1 = []
        emp_name = EmployeeDetails.objects.filter(emp_id=request.data.get('details', dict()).get('emp_id')) \
            .values_list('name')
        manager_name = EmployeeDetails.objects.filter(emp_id=request.data.get('details', dict()).get('manager_id')). \
            values_list('name')
        if emp_id and skills:
            for skill_details in skills:
                SkillEvaluation.objects.filter(emp_id=emp_id, skill_id=skill_details['skill_id'],
                                               evaluator_id=request.data.get('details', dict()).get('manager_id')). \
                    update(rating=skill_details['skill_rating'], evaluated_at=datetime.datetime.now())
                skill = Skills.objects.filter(skill_id=skill_details['skill_id'])
                skill = SkillSerializer(skill, many=True)
                data1 = skill.data
                for data1 in data1:
                    data1['skill_rating'] = skill_details['skill_rating']
                    list1.append(data1)
            return Response({"status": 'Skills Evaluated', 'emp_name': emp_name[0][0], 'emp_id': emp_id,
                             'manager_name': manager_name[0][0], 'skill_details': list1})
        return Response({"error": "Please enter all the required fields"})


class EmployeeSkills(APIView):
    """Used to display all the skills of an employee"""
    @staticmethod
    def post(request):
        list1 = []
        emp_id = request.data.get('emp_id', None)
        if emp_id:
            emp_details = EmployeeDetails.objects.filter(emp_id=emp_id).values('emp_id', 'name')
            emp_details = EmployeeSkillsSerializer(emp_details, many=True)
            emp_details = emp_details.data
            details = SkillEvaluation.objects.filter(emp_id_id=request.data.get('emp_id', None))
            skill_list = SkillEvaluationSerializer(details, many=True)
            data = skill_list.data
            for data in data:
                skill_name = Skills.objects.filter(skill_id=data['skill_id']).values_list('skill_name')
                data['skill_name'] = skill_name[0][0]
                evaluator_id = EmployeeDetails.objects.filter(emp_id=data['evaluator_id']).values_list('name')
                data['evaluator_name'] = evaluator_id[0][0]
                list1.append(data)
                emp_details[0]['skills'] = list1
            return Response({"emp_details": emp_details})
        return Response({"error": "Please enter all the required fields"})


class SkillList(APIView):
    """Displays all the company skills and all the employee who posses those skills"""
    @staticmethod
    def post(request):
        list1 = []
        list2 = []
        list3 = []
        skills = request.data.get('skills', None)
        if skills:
            for skill_id in skills:
                for value in skill_id.values():
                    skills = Skills.objects.filter(skill_id=value)
                    skills = SkillSerializer(skills, many=True)
                    list1.append(skills.data)
            for list1 in list1:
                list3.append(list1[0])
        else:
            skills = Skills.objects.all()
            skills = SkillSerializer(skills, many=True)
            list3 = skills.data
        for skills in list3:
            emp_list = SkillEvaluation.objects.filter(skill=skills['skill_id'], rating__isnull=False) \
                .values('emp_id', 'rating', 'evaluator_id', 'evaluated_at')
            for emp_list in emp_list:
                emp_id = emp_list['emp_id']
                emp_details = EmployeeDetails.objects.filter(emp_id=emp_id)
                emp_details = EmployeeSkillsSerializer(emp_details, many=True)
                for emp_details in emp_details.data:
                    emp_details['skill_rating'] = emp_list['rating']
                    emp_details['evaluator_id'] = emp_list['evaluator_id']
                    manager_name = EmployeeDetails.objects.filter(emp_id=emp_list['evaluator_id']).values_list(
                        'name')
                    emp_details['evaluator_name'] = manager_name[0][0]
                    emp_details['evaluated_at'] = emp_list['evaluated_at']
                    list2.append(emp_details)
                    skills['emp_details'] = list2
            list2 = []
        return Response({"skill_list": list3})


class ManagerSkillsRatingList(APIView):
    """Used to display all the skills which are evaluated by a particular manager"""
    @staticmethod
    def post(request):
        list1 = []
        manager_id = request.data.get('emp_id', None)
        skill_id = request.data.get('skill_id', None)
        ratings = request.data.get('ratings', None)
        if skill_id and ratings:
            ratings = SkillEvaluation.objects.filter(evaluator_id=manager_id, rating=ratings, skill=skill_id)
        elif skill_id:
            ratings = SkillEvaluation.objects.filter(evaluator_id=manager_id, rating__isnull=False, skill=skill_id)
        elif ratings:
            ratings = SkillEvaluation.objects.filter(evaluator_id=manager_id, rating=ratings)
        else:
            ratings = SkillEvaluation.objects.filter(evaluator_id=manager_id, rating__isnull=False)
        ratings = ManagerSkillRatingsSerializer(ratings, many=True)
        for ratings in ratings.data:
            emp_name = EmployeeDetails.objects.filter(emp_id=ratings['emp_id']).values_list('name')
            ratings['emp_name'] = emp_name[0][0]
            skill_name = Skills.objects.filter(skill_id=ratings['skill_id']).values_list('skill_name')
            ratings['skill_name'] = skill_name[0][0]
            list1.append(ratings)
        return Response({"emp_ratings": list1})


class Projects(APIView):
    """Used to display all the projects of an employee"""
    @staticmethod
    def post(request):
        emp_id = request.data.get('emp_id', None)
        list1 = []
        if emp_id:
            project_id = EmployeeProjects.objects.filter(emp_id=emp_id).values_list('project')
            for project_id in project_id:
                project_details = ProjectSerializer(ProjectsDetails.objects.filter(project_id=project_id[0]), many=True)
                for project_details in project_details.data:
                    manager_name = EmployeeDetails.objects.filter(emp_id=project_details['project_manager_id']). \
                        values_list('name')
                    project_details['manager_name'] = manager_name[0][0]
                    if EmployeeProjects.objects.filter(project_manager_id=request.data.get('emp_id', None)).exists()\
                            is True:
                        details = EmployeeProjects.objects.filter(
                            project_manager_id=request.data.get('emp_id', None), project_id=project_id[0])
                        details = ProjectEmployeeSerializer(details, many=True)
                        details = details.data[:]
                        for data in details:
                            for emp_details in EmployeeDetails.objects.filter(emp_id=data['emp_id']).values(
                                    'email', 'phone_number', 'name'):
                                data['emp_name'] = emp_details['name']
                                data['email_id'] = emp_details['email']
                                data['phone_number'] = emp_details['phone_number']
                            project_details['emp_list'] = list(details)
                        list1.append(project_details)
                    else:
                        list1.append(project_details)
            return Response({"projects_list": list1})
        return Response({"error": "Please enter all the required fields"})


class ProjectsList(APIView):
    """Used Get all the company projects and the employees who are working in the project"""
    @staticmethod
    def post(request):
        list1 = []
        list2 = []
        list3 = []
        project_id = request.data.get('project_details', None)
        if project_id:
            for project_id in project_id:
                for pro_details in project_id.values():
                    pro_details = ProjectsDetails.objects.filter(project_id=pro_details)
                    details = EmployeeProjectSerializer(pro_details, many=True)
                    list1.append(details.data)
            for list1 in list1:
                list3.append(list1[0])
        else:
            project_id = ProjectsDetails.objects.all()
            project_id = EmployeeProjectSerializer(project_id, many=True)
            list3 = project_id.data
        for details in list3:
            project_list = EmployeeDetails.objects.filter(emp_id=details['project_manager_id']). \
                values('name', 'email', 'phone_number')
            for project_list in project_list:
                details['manager_name'] = project_list['name']
                details['manager_email_id'] = project_list['email']
                details['manager_phone_number'] = project_list['phone_number']
                emp_id = EmployeeProjects.objects.filter(project_id=details['project_id'])
                emp_id = ProjectEmployeeSerializer(emp_id, many=True)
                emp_id = emp_id.data[:]
                for emp_id in emp_id:
                    for emp_details in EmployeeDetails.objects.filter(emp_id=emp_id['emp_id']). \
                            values('email', 'phone_number', 'name'):
                        emp_id['emp_name'] = emp_details['name']
                        emp_id['email_id'] = emp_details['email']
                        emp_id['phone_number'] = emp_details['phone_number']
                        list2.append(emp_id)
                        details['emp_details'] = list2
            list2 = []
        return Response({'project_details': list3})


class AddNewProjects(APIView):
    """Used to add new projects"""
    @staticmethod
    def post(request):
        project_name = request.data.get('project_name', None)
        project_manager = request.data.get('project_manager', None)
        started_on = request.data.get('started_on', None)
        #started_on = datetime.datetime.strptime(started_on, "%Y-%m-%d").date
        started_on = datetime.datetime.strptime(started_on, "%m/%d/%Y").strftime("%Y-%m-%d")
        reporting_manager = request.data.get('reporting_manager', None)
        primary_cost_center = request.data.get('primary_cost_centre', None)
        employees = request.data.get('employees', None)
        skills = request.data.get('skills', None)
        if project_name and project_manager:
            if ProjectsDetails.objects.filter(project_name=project_name).exists() is False:
                project = ProjectsDetails(project_name=project_name, project_manager_id=project_manager,
                                          started_on=started_on, project_status='On Going',
                                          reporting_manager_id_id=reporting_manager,
                                          primary_cost_center=primary_cost_center)
                project.save()
                manager_name = EmployeeDetails.objects.filter(emp_id=request.data.get('project_manager', None)). \
                    values_list('name')
                reporting_manager_name = EmployeeDetails.objects.filter(emp_id=request.data.get
                                                                        ('reporting_manager', None)).values_list('name')
                for employee_id in employees:
                    for value in employee_id.values():
                        project_id = ProjectsDetails.objects.filter(project_name=project_name).values_list('project_id',
                                                                                                           flat=True)[0]
                        details = EmployeeProjects(emp_id_id=value, project_id=project_id,
                                                   project_manager_id_id=project_manager)
                        details.save()
                for skills_id in skills:
                    for value in skills_id.values():
                        project_id = ProjectsDetails.objects.filter(project_name=project_name).values_list('project_id',
                                                                                                           flat=True)[0]
                        details = ProjectSkills(project_id=project_id, skill_id=value)
                        details.save()
                return Response({"status": "Project Added", "project_details": [
                    dict(project_name=request.data.get('project_name', None), project_manager=manager_name[0][0],
                         reporting_manager=reporting_manager_name[0][0])]})
            return Response({"status": project_name + ' already exists'})
        return Response({"error": "Please enter all the required fields"})


class AddEmployeeToProject(APIView):
    """Used to add employee to project"""
    @staticmethod
    def get(_):
        list1 = []
        skill_list = ""
        project_list = ""
        emp_details = EmployeeDetails.objects.filter(emp_id__icontains='AAPL/HR/00', status__isnull=True)
        emp_details = EmployeeSkillsSerializer(emp_details, many=True)
        for emp_details in emp_details.data:
            skill = SkillEvaluation.objects.filter(emp_id=emp_details['emp_id'], rating__isnull=False)
            skill = SkillEvaluationSerializer(skill, many=True)
            for skill in skill.data:
                skill_name = Skills.objects.filter(skill_id=skill['skill_id']).values_list('skill_name',
                                                                                           flat=True)[0]
                skill_list += skill_name + "-" + str(skill['rating']) + ","
            emp_details['skill'] = skill_list[:-1]
            skill_list = ''
            list1.append(emp_details)
            projects = EmployeeProjects.objects.filter(emp_id=emp_details['emp_id'])
            projects = ProjectEmployeeSerializer(projects, many=True)
            for projects in projects.data:
                project_name = ProjectsDetails.objects.filter(project_id=projects['project']).values_list(
                    'project_name', flat=True)[0]
                project_list += project_name + "-" + str(projects['project_contribution']) + ","
            emp_details['projects'] = project_list[:-1]
            project_list = ''
        list1.append(emp_details)
        return Response({"employee_details": list1})


class SearchAddEmployeeToProject(APIView):
    """Used to add employee to project"""
    @staticmethod
    def post(request):
        list1 = []
        list2 = []
        list3 = []
        skill_list = ""
        project_list = ''
        skills = request.data.get('skills', None)
        if skills:
            for skill_id in skills:
                for value in skill_id.values():
                    skills = Skills.objects.filter(skill_id=value)
                    skills = SkillSerializer(skills, many=True)
                    list1.append(skills.data)
            for list1 in list1:
                list3.append(list1[0])
        for skills1 in list3:
            emp_list = SkillEvaluation.objects.filter(skill=skills1['skill_id'], rating__isnull=False) \
                .values('emp_id', 'rating')
            for emp_list in emp_list:
                emp_id = emp_list['emp_id']
                emp_details = EmployeeDetails.objects.filter(emp_id=emp_id)
                emp_details = EmployeeSkillsSerializer(emp_details, many=True)
                for emp_details in emp_details.data:
                    skill = SkillEvaluation.objects.filter(emp_id=emp_details['emp_id'], rating__isnull=False,
                                                           skill_id=skills1['skill_id'])
                    skill = SkillEvaluationSerializer(skill, many=True)
                    for skill in skill.data:
                        skill_name = Skills.objects.filter(skill_id=skills1['skill_id']).values_list('skill_name',
                                                                                                     flat=True)[0]
                        skill_list += skill_name + "-" + str(skill['rating']) + ","
                    emp_details['skill'] = skill_list[:-1]
                    skill_list = ''
                    list1.append(emp_details)
                    projects = EmployeeProjects.objects.filter(emp_id=emp_details['emp_id'])
                    projects = ProjectEmployeeSerializer(projects, many=True)
                    for projects in projects.data:
                        project_name = ProjectsDetails.objects.filter(project_id=projects['project']).values_list(
                            'project_name', flat=True)[0]
                        project_list += project_name + "-" + str(projects['project_contribution']) + ","
                    emp_details['projects'] = project_list[:-1]
                    project_list = ''
                list2.append(emp_details)
        return Response({"employee_details": list2})


class AddNewProjectSkill(APIView):
    """Adding new skills which are needed for a project """
    @staticmethod
    def post(request):
        skill_name = request.data.get('skill', None)
        if skill_name:
            if Skills.objects.filter(skill_name=skill_name).exists() is False:
                skill = Skills(skill_name=skill_name)
                skill.save()
                return Response({"status": 'Skill ' + skill_name + ' Added'})
            else:
                return Response({"status": skill_name + ' already exists'})
        return Response({"error": "Please enter all the required fields"})


class CreateNewDesignation(APIView):
    """Adding new skills which are needed for a project """
    @staticmethod
    def post(request):
        designation_name = request.data.get('designation', None)
        if designation_name:
            if Designation.objects.filter(designation=designation_name).exists() is False:
                designation = Designation(designation=designation_name)
                designation.save()
                return Response({"status": 'Designation ' + designation_name + ' Added'})
            else:
                return Response({"status": designation_name + ' already exists'})
        return Response({"error": "Please enter all the required fields"})


class EmployeeList(APIView):
    """Used to display all the employees in the company"""
    @staticmethod
    def post(request):
        list1 = []
        emp_id = request.data.get('emp_id', None)
        if emp_id:
            details = EmployeeDetails.objects.filter(emp_id=emp_id)
        else:
            details = EmployeeDetails.objects.all()
        details = EmployeeSerializer(details, many=True)
        details = details.data[:]
        for details in details:
            designation_id = Promotion.objects.filter(emp_id=details['emp_id']).values_list('designation_id').\
                latest('date_of_promotion')
            for designation_name in Designation.objects.filter(designation_id=designation_id[0])\
                    .values_list('designation'):
                details['designation'] = designation_name[0]
            list1.append(details)
        return Response({'emp_details': list1})


class EmployeeCategory(APIView):
    """Used to categorize the employees"""
    @staticmethod
    def post(request):
        list1 = []
        category = request.data.get('category', None)
        category_id = SubRoles.objects.filter(sub_roles=category).values_list('sub_role_id')
        for category_id in category_id:
            emp_id = RoleAssignment.objects.filter(sub_role=category_id[0]).values_list('emp_id')
            for emp_id in emp_id:
                details = EmployeeDetails.objects.filter(emp_id=emp_id[0])
                details = EmployeeSerializer(details, many=True)
                list1.append(details.data[0])
            return Response({category + " list": list1})


class PasswordChange(APIView):
    """Used to change the password of a user"""
    @staticmethod
    def post(request):
        emp_id = request.data.get('emp_id')
        old_password = request.data.get('old_password')
        new_password = request.data.get('new_password')
        if emp_id and old_password and new_password:
            user_details = EmployeeDetails.objects.get(emp_id=emp_id)
            if check_password(old_password, user_details.password):
                user_details.set_password(new_password)
                user_details.save()
                return Response({"response": "Password Changed"})
            else:
                return Response({"response": "The entered Old Password is incorrect"})
        else:
            return Response({"error": "Please enter all the required fields"})


class InterviewRegistration(APIView):
    """Used to register candidates for interview"""
    @staticmethod
    def post(request):
        register = Registration(name=request.data.get('name'), email_id=request.data.get('email_id'),
                                phone_number=request.data.get('phone_number'), alternative_phone_number=request.data.
                                get('alternative_phone_number'), gender=request.data.get('gender'),
                                date_of_birth=request.data.get('date_of_birth'), address=request.data.get('address'),
                                father_name=request.data.get('father_name'), qualification=request.data.get
                                ('qualification'), skills=request.data.get('skills'), experience_in=request.data.get
                                ('experience'), interested_field=request.data.get('interested_field'))
        register.save()
        return Response({"status": "1", "message": "Registered Successfully"})


class InterviewScheduling(APIView):
    """Used to differentiate candidates for learners and employment"""
    @staticmethod
    def get(_):
        learners = Registration.objects.filter(experience_in__isnull=True, final_result__isnull=True)
        learners = RegistrationSerializer(learners, many=True)
        employee = Registration.objects.filter(experience_in__isnull=False, final_result__isnull=True)
        employee = RegistrationSerializer(employee, many=True)
        return Response({"learners": learners.data, "employee": employee.data})


class Email(APIView):
    @staticmethod
    def post(request):
        message = request.data.get('message', None)
        send_mail('subject', message, 'ambertagDjango@gmail.com',
                  ['guruprasad.sglc@gmail.com'])
        return Response("Message Sent")


class InterviewEmail(APIView):
    @staticmethod
    def post(request):
        """email = request.data.get('email', None)"""
        message = request.data.get('message', None)
        send_mail('Interview', message, 'ambertagDjango@gmail.com',
                  ['guruprasad.sglc@gmail.com'])
        return Response({"status": "Message Sent"})


@permission_classes((AllowAny,))
class Logout(APIView):
    """Used to destroy user tokens"""
    @staticmethod
    def post(request):
        emp_id = request.data.get("emp_id", None)
        Token.objects.filter(user_id=emp_id).delete()
        return Response({"status": "You have successfully logged out"})


@permission_classes((AllowAny,))
class CreateNewUsers1(APIView):
    @staticmethod
    def get(_):
        if EmployeeDetails.objects.filter(password__isnull=True).exists() is True:
            for user_details in EmployeeDetails.objects.filter(password__isnull=True, status__isnull=True,
                                                               email__isnull=False).values('email', 'emp_id'):
                user = EmployeeDetails.objects.get(email=user_details['email'])
                new_password = BaseUserManager().make_random_password()
                user.set_password(new_password)
                user.save()
                p = Password(email=user_details['email'], password=new_password, emp_id=user_details['emp_id'])
                p.save()
                """send_mail('subject', "password" + new_password, 'ambertagDjango@gmail.com', 
                          ['guruprasad.sglc@gmail.com'])"""
        return Response("Users Created")


class CreateNewUser(APIView):
    @staticmethod
    def post(request):
        emp_id = request.data.get('emp_id', None)
        email = request.data.get('email', None)
        designation = request.data.get('designation_id', None)
        date_of_birth = request.data.get('date_of_birth', None)
        date_of_birth = datetime.datetime.strptime(date_of_birth, "%m/%d/%Y").strftime("%Y-%m-%d")
        date_of_joining = request.data.get('date_of_joining', None)
        date_of_joining = datetime.datetime.strptime(date_of_joining, "%m/%d/%Y").strftime("%Y-%m-%d")

        if EmployeeDetails.objects.filter(emp_id=emp_id).exists() is False:
            if EmployeeDetails.objects.filter(email=email).exists() is False:
                new_data = EmployeeDetails(emp_id=request.data.get('emp_id'), first_name=request.data.get('first_name'),
                                           name=request.data.get('employee_name'),
                                           qualification=request.data.get('qualification'),
                                           date_of_joining=date_of_joining,
                                           gender=request.data.get('gender'),
                                           date_of_birth=date_of_birth,
                                           permanent_address=request.data.get('permanent_address'),
                                           resident_address=request.data.get('residential_address'),
                                           blood_group=request.data.get('blood_group'),
                                           pan_number=request.data.get('pan_number'),
                                           aadhar_number=request.data.get('aadhar_number'),
                                           alternative_phone_number=request.data.get('alternative_phone_number'),
                                           phone_number=request.data.get('phone_number'),
                                           father_name=request.data.get('father_name'),
                                           father_number=request.data.get('father_number'),
                                           mother_name=request.data.get('mother_name'),
                                           mother_number=request.data.get('mother_number'),
                                           emergency_contact_number=request.data.get('emergency_contact_number'),
                                           email_id_personal=request.data.get('email_id_personal'),
                                           email=request.data.get('email'),
                                           marital_status=request.data.get('marital_status'),
                                           document=request.data.get('documents', None),
                                           working_period_of_timings=request.data.get('working_period_of_timings'),
                                           is_active=1, is_staff=0)
                new_data.save()
                sub_role = 1
                if "AAPL/HR/C" in emp_id:
                    sub_role = 5
                elif "AAPL/HR/T" in emp_id:
                    sub_role = 3
                elif "SGLC/HR/L" in emp_id:
                    sub_role = 4
                role = RoleAssignment(emp_id_id=emp_id, sub_role_id=sub_role)
                role.save()
                promotion = Promotion(emp_id_id=emp_id, designation_id_id=designation,
                                      date_of_promotion=request.data.get('date_of_joining'))
                promotion.save()
                user = EmployeeDetails.objects.get(emp_id=emp_id)
                new_password = BaseUserManager().make_random_password()
                user.set_password(new_password)
                user.save()
                p = Password(email=email, password=new_password, emp_id=emp_id)
                p.save()
                send_mail('subject', "Email_Id: " + email + "Password: " + new_password, 'ambertagDjango@gmail.com',
                          ['guruprasad.sglc@gmail.com'])
                return Response({"status": "New Employee Added"})
            return Response({"status": "Email_id_official already taken"})
        return Response({"status": "Employee Id already taken"})


class InterviewResults(APIView):
    @staticmethod
    def post(request):
        interview = Interview(candidate_name=request.data.get("candidate_name"), email_id=request.data.get('email_id'),
                              experience=request.data.get('experience'), technical_knowledge=request.data.get
                              ('technical_knowledge'), verbal_communication_skills=request.data.get
                              ('verbal_communication_skills'), type_of_employment=request.data.get
                              ('type_of_employment'), notice_period_time=request.data.get('notice_period_time'),
                              comments=request.data.get('comments'), attitude=request.data.get('attitude'),
                              final_recommendation=request.data.get('final_recommendation'),
                              average_rating=request.data.get('average_rating'), candidate_phone_number=request.data.get
                              ('candidate_phone_number'), interview_date=datetime.datetime.now())
        interview.save()
        return Response({"status": "Candidate Registered"})


class GetInterviewers(APIView):
    @staticmethod
    def get(_):
        list1 = []
        interviewers = RoleAssignment.objects.filter(sub_role=7).values_list('emp_id')
        for emp_details in interviewers:
            emp_details = EmployeeDetails.objects.filter(emp_id=emp_details[0])
            emp_details = EmployeeSkillsSerializer(emp_details, many=True)
            emp_details = emp_details.data
            email = EmployeeDetails.objects.filter(emp_id=emp_details[0]['emp_id']).values_list('email')
            emp_details[0]['email_id'] = email[0][0]
            list1.append(emp_details[0])
        return Response({"interviewers_list": list1})


class IntervieweesList(APIView):
    """Used to display all the candidates who have """
    @staticmethod
    def get(_):
        interview_results = Interview.objects.all()
        interview_results = InterviewSerializer(interview_results, many=True)
        return Response({"interview_results": interview_results.data})


class EditProfile(APIView):
    """Used to edit employee profile"""
    @staticmethod
    def post(request):
        emp_id = request.data.get("emp_id", None)
        EmployeeDetails.objects.filter(emp_id=emp_id).update(phone_number=request.data.get("phone_number"),
                                                             father_number=request.data.get("father_number"),
                                                             mother_number=request.data.get("mother_number"),
                                                             marital_status=request.data.get("marital_status"),
                                                             email_id_personal=request.data.get("email_id_personal"),
                                                             blood_group=request.data.get("blood_group"),
                                                             emergency_contact_number=request.data.get
                                                             ("emergency_contact_number"),
                                                             resident_address=request.data.get("resident_address"),
                                                             alternative_phone_number=request.data.get
                                                             ("alternative_phone_number"))
        details = EmployeeDetails.objects.filter(emp_id=emp_id)
        details = EmployeeSerializer(details, many=True)
        details = details.data
        return Response({"status": "Profile Edited", "emp_details": details})


class HrEditProfile(APIView):
    """Used to edit employee profile in hr page"""
    @staticmethod
    def post(request):
        emp_id = request.data.get('emp_id', None)
        designation = request.data.get('designation', None)
        date_of_promotion = request.data.get('date_of_promotion')
        date_of_promotion = datetime.datetime.strptime(date_of_promotion, "%m/%d/%Y").strftime("%Y-%m-%d")
        date_of_joining = request.data.get('date_of_joining', None)
        date_of_joining = datetime.datetime.strptime(date_of_joining, "%m/%d/%Y").strftime("%Y-%m-%d")
        date_of_resigning = request.data.get('date_of_resigning', None)
        date_of_resigning = datetime.datetime.strptime(date_of_resigning, "%m/%d/%Y").strftime("%Y-%m-%d")
        date_of_rejoining = request.data.get('date_of_rejoining', None)
        date_of_rejoining = datetime.datetime.strptime(date_of_rejoining, "%m/%d/%Y").strftime("%Y-%m-%d")
        date_of_birth = request.data.get('date_of_birth', None)
        date_of_birth = datetime.datetime.strptime(date_of_birth, "%m/%d/%Y").strftime("%Y-%m-%d")
        months_on_job = request.data.get('months_on_job', None)
        if date_of_joining == "":
            date_of_joining = None
        if date_of_resigning == "":
            date_of_resigning = None
        if date_of_rejoining == "":
            date_of_rejoining = None
        if date_of_birth == "":
            date_of_birth = None
        if months_on_job == "":
            months_on_job = None
        EmployeeDetails.objects.filter(emp_id=emp_id).update(emp_id=request.data.get('emp_id'),
                                                             first_name=request.data.get('first_name'),
                                                             qualification=request.data.get('qualification'),
                                                             date_of_joining=date_of_joining,
                                                             months_on_job=months_on_job,
                                                             date_of_resigning=date_of_resigning,
                                                             date_of_rejoining=date_of_rejoining,
                                                             gender=request.data.get('gender'),
                                                             date_of_birth=date_of_birth,
                                                             permanent_address=request.data.get('permanent_address'),
                                                             resident_address=request.data.get('residential_address'),
                                                             blood_group=request.data.get('blood_group'),
                                                             pan_number=request.data.get('pan_number'),
                                                             aadhar_number=request.data.get('aadhar_number'),
                                                             alternative_phone_number=request.data.get
                                                             ('alternative_phone_number'),
                                                             phone_number=request.data.get('phone_number'),
                                                             father_name=request.data.get('father_name'),
                                                             father_number=request.data.get('father_number'),
                                                             mother_name=request.data.get('mother_name'),
                                                             mother_number=request.data.get('mother_number'),
                                                             emergency_contact_number=request.data.get
                                                             ('emergency_contact_number'),
                                                             email_id_personal=request.data.get('email_id_personal'),
                                                             email=request.data.get('email'),
                                                             marital_status=request.data.get('marital_status'),
                                                             document=request.data.get('documents', None),
                                                             status=request.data.get('status'),
                                                             working_period_of_timings=request.data.get
                                                             ('working_period_of_timings'))
        details = EmployeeDetails.objects.filter(emp_id=emp_id)
        details = EmployeeSerializer(details, many=True)
        details = details.data
        details[0]['profile_status'] = "Profile Edited"
        if designation and date_of_promotion:
            designation_id = Designation.objects.filter(designation=designation).values_list('designation_id')
            promotion = Promotion(emp_id_id=emp_id, designation_id_id=designation_id[0][0],
                                  date_of_promotion=date_of_promotion)
            details[0]['designation'] = request.data.get('designation', None)
            details[0]['date_of_promotion'] = date_of_promotion
            promotion.save()
        return Response(details[0])
